    /** Matchnet Server file **/

/** This dotenv module is used to seperate common things in this file **/
const dotenv = require('dotenv').config();

/** Require express modules and create server **/
var express = require('express');
var engine = require('ejs-locals');
var session = require('express-session');
var adminRoutes = require('./routes/admin')
var flash = require('connect-flash');
var url = require('url');

app = express(),
server = require('http').createServer(app),
io = require('socket.io')(server),
// get an instance of the router for api routes
apiRoutes = express.Router(),

logger = require('morgan'),
errorhandler = require('errorhandler'),
helmet = require('helmet'),

/** Set Port **/
port = process.env.port || 30080,


/** This module work as middleware to parse all request **/
bodyParser = require('body-parser'),

/** This module word as ORM for database **/
mongoose = require('mongoose'),

/** This module is used to handle asynchrous behavious of nodejs i.e avoiding callback hell **/
waterfall = require('async-waterfall'),

/* async is powerful functions for working with asynchronous JavaScript*/
async = require("async"),

/** This module is used to handle validation **/
validator = require('validator'),

/** require password-hash module for create secure password **/
passwordHash = require('password-hash'),

/** require nodemailer module to send emails **/
nodemailer = require('nodemailer'),

/** require nodemailer module to send mail smtp transport **/
smtpTransport = require('nodemailer-smtp-transport'), // 

/** require jsonwebtoken to authenticate the different routes  **/
jwt = require('jsonwebtoken'),

/** require to locate the path  **/
path = require("path"),

/** require encryptor module to encrypt or decrypt string **/
encryptor = require('simple-encryptor')(process.env.key),

/** require multer module to upload file **/
multer = require('multer'),

/** require fs module to handle file(s) **/
fs = require('fs'),

_ = require('lodash'),

MOMENT = require('moment'),

TronWeb = require('tronweb'),
keccak256 = require('keccak256'),
numberToBN = require('number-to-bn'),
abi = require('ethereumjs-abi'),

/** require cors module to handle cors **/
cors = require('cors'),

otpGenerator = require('otp-generator'),
cron = require('node-cron'),
BigNumber = require('bignumber.js'),

Cryptr = require('cryptr'),
cryptr = new Cryptr(process.env.secret),

rateLimit = require("express-rate-limit");

const reqLimiter = rateLimit({
  windowMs: 1 * 60 * 1000, // 1 minutes
  max: 150 // limit each IP to 150 requests per windowMs
});
app.use(reqLimiter);

var Ddos = require('ddos');
var ddos = new Ddos({burst:40, testmode:false, maxexpiry: 10});
app.use(ddos.express);

global.io = io; //added
require('./utils/socket')(io, cryptr);

/** Database connection **/
const dbOptions = {
  autoIndex: false, // Don't build indexes
  reconnectTries: 30, // Retry up to 30 times
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  useNewUrlParser: true
};
mongoose.Promise = global.Promise;

console.log(process.env.NODE_ENV);
if(process.env.NODE_ENV == "development"){
  mongoose.connect(process.env.database,dbOptions);
} else {
  mongoose.connect(process.env.staging_database,dbOptions);
}

var myDB = mongoose.connection;
// Retry connection
const connectWithRetry = () => {
  if(process.env.NODE_ENV == "development"){
    mongoose.connect(process.env.database,dbOptions);
  } else {
    mongoose.connect(process.env.staging_database,dbOptions);
  }
  myDB = mongoose.connection;
};
myDB.on("error", function(err){
  console.log("MY DB has an error with Mongoose", err);
  setTimeout(connectWithRetry, 5000);
})

//Check if successful connection is made
myDB.once("open", function callback() {
  console.log("MY DB Connected with Mongoose");
});

app.use(helmet());

// app.disable('x-powered-by')
app.use(helmet.contentSecurityPolicy({
  directives: {
    defaultSrc: ["'self'"],
    styleSrc: ["'self'", "'unsafe-inline'", 'fonts.googleapis.com'],
    fontSrc: ["'self'", 'fonts.gstatic.com'],
    scriptSrc: ["'self'", "'unsafe-inline'", 'ajax.googleapis.com', 'cdnjs.cloudflare.com', 'maxcdn.bootstrapcdn.com'],
  }
}))

app.engine('ejs', engine);
app.set('views', path.join(__dirname, 'template'));
app.set('view engine', 'ejs');

var expiryDate = new Date(Date.now() + 24 * 60 * 60 * 1000) // 24 * 1 hour
app.use(session({
  secret: process.env.SESS_SECRET,
  name: process.env.SESS_NAME,
  resave: true,
  saveUninitialized: true,
  cookie: {
    // secure: true,
    // httpOnly: true,
    // domain: process.env.BasePath,
    expires: expiryDate
  }
}))

/* parse application/json */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(flash());

// route to show a random message
apiRoutes.get('/', function(req, res) {
 res.json({ message: 'Welcome to the coolest API on earth!' });
});

app.get('/admin/', function(req, res, next) {
  const user =  req.session.user,
   userId = req.session.userId;
   if(userId == null){
      res.redirect("/admin/login");
      return;
   }
   else
   {
    res.render('index', {page:'Home', title:'home'});
   }
});


/** Require all models **/
const user = require('./models/user');
const game = require('./models/game');
const sponsor = require('./models/sponsor');
const bet = require('./models/bet');
const tournament = require('./models/tournament');
const point = require('./models/points');
const challenge = require('./models/challenge');
const revealCommit = require('./models/revealCommit');


require('./routes/admin/index')(app, user, game, sponsor, bet, multer, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, _);
require('./routes/admin/tournament')(app, user, game, sponsor, point, tournament, bet, multer, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, io, _);
require('./routes/admin/challenge')(app, user, challenge, multer, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, io, _);
app.use('/api', apiRoutes);


const allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('x-Trigger', 'CORS');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  next();
};
app.use(allowCrossDomain);


/* get all data/stuff of the body (POST) parameters */
var whitelist = process.env.whitelistArr;
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  },
  optionsSuccessStatus: 200 
}

app.use(cors(corsOptions));

/* parse application/vnd.api+json as json */ 
//app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 

/** Making public folder to be accebile globally **/
app.use(express.static(__dirname + '/public')); 

if(process.env.NODE_ENV === 'development' ){
  app.use(logger('dev'));
}
else{
  app.use(logger('Prod'));
}

app.use(errorhandler());

/** Vidit -- Middle ware for cross checking the header client domain */
/** any request without this client domain will not be entertained */
const response = require("./utils/response");
const clientDomainValidator = (req,res,next) => {
  let headers = req.headers;

  const validDomains = process.env.CLIENT_DOMAINS.split(",");
  if (!validDomains.includes(headers.clientdomain)) 
  {
    return res.send(
      response.failure("Unauthorize: Invalid Request")
    )
  }
  next();
}
// app.use(clientDomainValidator);
/** --------------------------------------------------------- */
app.use(function(req, res, next) {
  var urlParts = url.parse(req.url, true);
  let allowedUrl = ['/sso','/login', '/api'];
  console.log('urlParts', urlParts)
  const isAllowedUrl = allowedUrl.find( urlStr => (urlStr === urlParts.pathname || urlParts.pathname.indexOf("api") >= 0));
  if(!isAllowedUrl){
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {

      const decryptedtoken = cryptr.decrypt(token);

      jwt.verify(decryptedtoken, process.env.key, function(err, decoded) {
        if (err) {
          //Here I can check if the received token in the request expired
          if(err.name == "TokenExpiredError"){
              var refreshedToken = jwt.sign({
                  success: true,
                  }, process.env.key, {
                      expiresIn: process.env.tokenexp
                  });
              req.apiToken = refreshedToken;
            }else if (err) {
              return res.json({ success: false, message: 'Failed to authenticate token.' });
            }         
        } else {
          //If no error with the token, continue 
          req.apiToken = token;
        };
      });
    } else {
      return res.status(403).send({
          success: false,
          message: 'No token provided.'
      });
    }
  }
  next();
});

/** Default routes for admin login **/
//require('./routes/Admin/default_routes')(app, path);

/** Function to send custom message **/
function customValidationMsg(message, res)
{
  const json_arr  = {data: [{status: 0, message : message}], success: false };
  res.contentType('application/json');
  res.end(JSON.stringify(json_arr));
}

/**Common Code to upload Image files **/
const upload = multer({ dest: 'public/uploads/profile' });
app.post('/imageupload', upload.single('image'), function(req, res){
  const filedata = req.file;
  if(filedata)
  {
    if(req.body.oldimage)
    {
      fs.unlink('public/uploads/profile/'+req.body.oldimage, function(err, response){
      });
    }
    const result = {status : 1, 'data' : filedata.filename,  error:'', message : ''};
    const json_arr  = {data: result, success: true};
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
  }
  else
  {
    const result = {status : 0, 'data' : '',  error:'', message : 'Unable to upload file.'};
    const json_arr  = {data: result, success: false};
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
  }
})

/**Middleware to Check if a User is Authenticated **/
function isAuthenticated(req, res, next) {
  // do any checks you want to in here
  // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
  // you can do this however you want with whatever variables you set up
  if (req.session && req.session.userId)
    return next();

  // IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM SOMEWHERE
  res.redirect('/admin/login');
}

/************************************************************ API Routes ***************************************************************************/
/**Code for signup/login/verification which accept data as json in request and return response accordingly **/
require('./routes/api/auth')(app, user, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, cryptr, _);
require('./routes/api/bet')(app, user, bet, challenge, point, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, io, _);
require('./routes/api/challenge')(app, io, user, bet, point, challenge, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, _, TronWeb);
require('./routes/api/tournament')(app, user, bet, point, challenge, tournament, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, _);
require('./routes/api/sponsor')(app, sponsor, multer, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, cryptr, _);
require('./routes/api/game')(app, game, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, cryptr, _);
/***************************************************************************************************************************************************/

/************************************************************ SmartContract ***************************************************************************/
require('./routes/smartContract')(app, TronWeb, revealCommit);
require('./routes/getCommit')(app, revealCommit, keccak256, numberToBN, abi );
/**********************************************************************************************************************************************/
/************************************************************ Cron ***************************************************************************/
var crontab = require('./utils/cron')(app, cron, io, tournament, bet, point);
require('./routes/cron/scripts/dailyReward')(app, cron, TronWeb, BigNumber);
/*****************************************************************************************************************************************************/


server.listen(port, function  (argument) {
  console.log('Server listening on : ' + port);    
})