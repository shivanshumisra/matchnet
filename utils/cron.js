exports = module.exports = function(app, cron, io, tournament, bet, point) {
  var prizeMatrix = require('./prizeMatrix');
  cron.schedule('*/3 * * * *', () => {
    console.log('Execute below event once a day at midnight 00:05');
    function sendPublishTournament(no_of_tournament, names) {
      global.io.emit("tournament_published", {no_of_tournament: no_of_tournament, details:names, status:'Ongoing', message: "tournament published"});
    };
    /** Publish tournament : start **/
    function publishTournament(){
      return new Promise(function(resolve, reject){

        // let startDate = new Date();
        let startDate = new Date().toLocaleString('en-US', {'timeZone': 'Asia/Calcutta'});
        // startDate.setHours(0,0,0,0);

        let endDate = new Date(); 
        // endDate.setHours(23,59,59,999);
        let myquery = {status: 'Upcoming', startdate:  {"$lt": new Date(startDate)}};
        // let myquery = {status: 'Upcoming', startdate: startDate.toUTCString()}; 
        // console.log('myquery', myquery);   
        let jsondata = {status:'Ongoing'}
        tournament.updateTournamentByQuery(myquery, jsondata, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }  
    function getTournament(){
      
      return new Promise(function(resolve, reject){

        let startDate = new Date().toLocaleString('en-US', {'timeZone': 'Asia/Calcutta'});
        // startDate.setHours(0,0,0,0);

        var endDate = new Date();
        // endDate.setHours(23,59,59,999);
        // let myquery = {status: 'Upcoming', startdate:  {"$gte": startDate.toUTCString() , "$lt": endDate.toUTCString() }};
        let myquery = {status: 'Upcoming', startdate:{"$lt": new Date(startDate)}}; 
        tournament.getTournamentByStatus(myquery, {},  function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    var getTournament = getTournament();
    var publishTournament = publishTournament();
    Promise.all([getTournament, publishTournament]).then(function(response) {
      if(response[1].nModified > 0){
        let tNames = [];
        _.forEach(response[0].tournaments, function(value, key) {
          tNames.push(value.name);
        })
        sendPublishTournament(tNames.length, tNames);
      }
    })
    .catch(function(derr,err){
      console.log('derr,err', derr,err)
    })
    /** Publish tournament : end **/ 
    
    /** get tournament : start **/
    function getTournaments(){
      return new Promise(function(resolve, reject){

        let startDate = new Date();
        // startDate.setHours(0,0,0,0);

        // let endDate = new Date();
        let endDate = new Date().toLocaleString('en-US', {'timeZone': 'Asia/Calcutta'});
        // endDate.setHours(23,59,59,999);
        let myquery = {status: 'Ongoing', enddate:  {"$lt": new Date(endDate) }};
        // let myquery = {status: 'Ongoing', enddate: endDate.toUTCString()};
        tournament.getTournamentByStatus(myquery, {}, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function getBetDetailByTournamentId(tournamentId){
      return new Promise(function(resolve, reject){    
        bet.getBetsByTournamentId(tournamentId, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function saveWinner(tournamentId, playerId) {
      return new Promise(function(resolve, reject){
        tournament.updateWinner(tournamentId, playerId, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
              resolve(response);
            }
        })
      })
    }
    function getPointDetailByTournamentId(tournamentId){
      return new Promise(function(resolve, reject){    
        point.getMaxPointDetailByTournamentId(tournamentId, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function calculateWinnerAmount(winPlayer, totalWinPlayer, playerArr, noOfPlayer, enrolledPlayer, winningAmount, entryFee){
      let prizeAmt = 0;
      if(entryFee == 0){
        prizeAmt = winningAmount
      }
      if(entryFee > 0){
        prizeAmt = (enrolledPlayer.length * entryFee)*.95;
      }
      if(entryFee==0){
        let prizeAmt = winningAmount*.95;
      }
      if(winPlayer.length > 0){
        _.forEach(playerArr, function(svalue, skey) {
          let playerWinAmt = prizeAmt * (totalWinPlayer[skey]/100);
          playerArr[skey].win_amount = Math.round(playerWinAmt);
          playerArr[skey].win_percentage = totalWinPlayer[skey];
          playerArr[skey].user_position = (skey + 1);
        })
      }
      return playerArr;
    }
    getTournaments()
      .then(function(response){
        _.forEach(response.tournaments, function(value, key) {
          var tournamentid = value._id;
          getPointDetailByTournamentId(tournamentid)
          .then(function(res){

            let getWinIds = [];
            let totalWinPlayer = [];
            if(value.noOfPlayer > 0){
              totalWinPlayer = prizeMatrix.calculatePrize(value.noOfPlayer);
              if(res.length > 0){
                let tmpWinArr = [];
                if(res.length > totalWinPlayer.length){
                  tmpWinArr = res.slice(totalWinPlayer.length);
                }
                else
                {
                  tmpWinArr = res;
                }
                if(tmpWinArr.length > 0){
                  getWinIds = tmpWinArr.map(player => player.playerId);
                }
              }
            }
            //calculate winner
            let winPlayer = (getWinIds && getWinIds.length > 0) ? getWinIds : [];
            let tmpresponse = (winPlayer) ? res : value;
            let noOfPlayer = value.noOfPlayer;
            let enrolledPlayer = value.players;
            let winningAmount = value.winning_amount;
            let entryFee = value.entry_fee;
            if(winPlayer.length > 0){
              winPlayer = calculateWinnerAmount(winPlayer, totalWinPlayer, tmpresponse, noOfPlayer, enrolledPlayer, winningAmount, entryFee);
            }
            saveWinner(tournamentid, winPlayer)
            .then(function(resp){
              global.io.emit("tournament_finished", {data:tmpresponse, message: "tournament finished"});
            })
          });
        });
      })
      .catch(function(derr,err){
        console.log('derr,err', derr,err)
      })
    /** finish tournament : end **/  
  });
}