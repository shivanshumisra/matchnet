exports = module.exports = function(io, cryptr){
  const tokenValidator = (socket,next) => {
    /** client side usage */
    // const socket = io('http://localhost?token=abc'); //
    let token = socket.handshake.query.token;
    if (token) {
      const decryptedtoken = cryptr.decrypt(token);
      jwt.verify(decryptedtoken, process.env.key, function(err, decoded) {
        if (err) {
          //Here I can check if the received token in the request expired
          if(err.name == "TokenExpiredError"){
              const refreshedToken = jwt.sign({
                  success: true,
                  }, process.env.key, {
                      expiresIn: process.env.tokenexp
                  });
              // request.apiToken = refreshedToken;
               next();
            }else if (err) {
              //return res.json({ success: false, message: 'Failed to authenticate token.' });
               next(new Error('socket authentication error'));
            }         
        } else {
          //If no error with the token, continue 
          // request.apiToken = token;
           next();
        };
      });
    } else {
       next(new Error('socket authentication error'));
       //next();
    }
    
  }
  io.use(tokenValidator);
  io.on('connection', (socket) => {
    console.log('a user connected');
    socket.emit('welcome', { message: 'Welcome! test', id: socket.id });
    socket.on('i am client', console.log);

    socket.on('message',data => {
      socket.emit('new message',{data: data, message: data.message});
    });

    socket.on('challenge_published',data => {
      console.log('challenge_published',data)
      socket.emit('challenge_published',{data: data, message: data.message});
    });

    socket.on('tournament_published',data => {
      console.log('tournament_published',data)
    });
    socket.on('tournament_finished',data => {
      console.log('tournament_finished',data)
    });

  }, error => {
    console.log(error);
  });

  io.on('error', (socket) => {
    console.log(error);
  })
}