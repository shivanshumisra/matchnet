const mongoose = require('mongoose');

const prize = new mongoose.Schema({
    prize: {
        type: { type: String }, 
        amount: { type: Number },
        name: { type: String },
        description: { type: String },
        image: {type : String},
        for_ranks: [{ type: Number }]
    }
});

var players = mongoose.Schema({
    playerId : {type: mongoose.Schema.ObjectId, ref: 'user', required : true},
    enroll_date: { type : Date, default : Date.now()},
    playerBets : [{type: mongoose.Schema.ObjectId, ref: 'Bet'}]
  });

const TournamentSchema  = mongoose.Schema({
  
  gameid      : {type : mongoose.Schema.ObjectId, ref: 'Game', required : true},
  name        : { type : String, required : true, unique : true },
  entry_fee   : { type: Number , default: 0},
  logo        : { type : String}, 
  description : { type : String },
  manual      : { type : Boolean},
  startdate   : { type : Date, default : null},
  enddate     : { type : Date, default: null},
  status      : { type : String, enum : ['Ongoing', 'Upcoming', 'Finished'], default : 'Upcoming' },
  prizes      : [prize],
  players     : [players],
  winners     :  [ {playerId: {type: mongoose.Schema.ObjectId, ref: 'user'}, win_amount: {type: Number }, win_percentage: {type: String}, user_position: {type: String}} ],
  team        :   {teamId: {type: mongoose.Schema.ObjectId, ref: 'Team'}},
  sponsorid   :   {type: mongoose.Schema.ObjectId, ref: 'Sponsor'},
  noOfPlayer   : { type : Number, default : 0},
  winning_amount  : { type : Number, default : 0},
  created_on  : { type : Date, default : Date.now()}
})

const Tournament = module.exports = mongoose.model('Tournament', TournamentSchema); 

module.exports.getAllTournaments = function(condData = {}, fields= {}, callback){
  var gameOptions = {
      path: 'gameid',
      select: " name logo description genre _id",
  };
  var sponsorOptions = {
      path: 'sponsorid',
      select: " name logo description banner_ads_1 banner_ads_2 _id",
  };
  var playerOptions = {
        path: 'players.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
    var winnerOptions = {
        path: 'winners.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  Tournament.find(condData, fields)
  .populate(gameOptions).populate(sponsorOptions).populate(playerOptions).populate(winnerOptions)
  .sort('-created_on')
  .exec(function(err, tournaments) {
    callback(null, {
      tournaments: tournaments,
    });
  })
}

module.exports.createData = function(TournamentData, callback){
  Tournament.create(TournamentData, function(err, tournament){
    if(err){
      callback(err);
    }
    else
    {
      callback(null, tournament);
    }
  })
}

module.exports._updateTournament = function(_tournamentId, userId, _bestScore, _bestScoreDate, _score, type, callback){
  if(type == "exist")
  {
    Tournament.update({_id: _tournamentId, 'players.playerId': userId }, {

       $set: {
                  'players.$.best_score.score': _bestScore,
                  'players.$.best_score.datetime': _bestScoreDate,
                  'players.$.playerId': userId
              },
              $push: {
                  'players.$.scores': {
                      score: _score,
                      datetime: new Date()
                  }
    }}, {new:true}).exec(function(err, data){
      if(err)
      {
        callback(err);
      }
      else
      {
        callback(null, data);
      }
    })
  }
  else
  {
    Tournament.update({_id: _tournamentId}, {
        $push: {
                      players: {
                          playerId: userId,
                          scores: [{
                              score: _score
                          }],
                          best_score: {
                              score: _score
                          }
                      }
                  }}, {upsert: true}).exec(function(err, data){
      if(err)
      {
        callback(err);
      }
      else
      {
        callback(null, data);
      }
    })
  }

}

module.exports.updatePlayer = function(_whereId, userId, callback){
  
  Tournament.update({'players.playerId': _whereId }, {

     $set: {                
                'players.$.playerId': userId
            }}, {multi:true}).exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
  
}

module.exports.updateWinner = function(_tournamentId, userIds, callback){
  let rawData = {
      'status': 'Finished',
      'enddate': new Date().toUTCString()
    }
  if(userIds){
    let tmpWinner = [];
    _.forEach(userIds, function(value, key) {
      tmpWinner.push({'playerId' : mongoose.Types.ObjectId(value.playerId), 'win_amount' : value.win_amount, 'win_percentage' : value.win_percentage, 'user_position' : value.user_position})
    })
    rawData = {
      'status': 'Finished',
      'enddate': new Date().toUTCString(),
      'winners': tmpWinner
    }
  }
  Tournament.updateOne({'_id': mongoose.Types.ObjectId(_tournamentId)}, rawData, {upsert: true}).exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}

module.exports.playTournament = function(_tournamentId, userId, betId, callback){

  Tournament.updateOne({'_id': mongoose.Types.ObjectId(_tournamentId), 'players.playerId': mongoose.Types.ObjectId(userId)}, {
      $push: {'players.$.playerBets' : betId}
    }).exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}

module.exports.getTournamentData = function(id, callback){
  var gameOptions = {
      path: 'gameid',
      select: " name logo description genre _id",
  };
  var sponsorOptions = {
      path: 'sponsorid',
      select: " name logo description banner_ads_1 banner_ads_2 _id",
  };
  var playerOptions = {
        path: 'players.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
    var winnerOptions = {
        path: 'winners.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  Tournament.find({'userid' : id}).populate(gameOptions).populate(sponsorOptions).populate(winnerOptions).populate(playerOptions).sort('-created_on').lean().exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}


module.exports.getTournamentInfoById = function(id, callback){
   var gameOptions = {
      path: 'gameid',
      select: " name logo description genre _id",
  };
  var sponsorOptions = {
      path: 'sponsorid',
      select: " name logo description banner_ads_1 banner_ads_2 _id",
  };
  var playerOptions = {
        path: 'players.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
    var winnerOptions = {
        path: 'winners.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  Tournament.findOne({_id: mongoose.Types.ObjectId(id)}).populate(gameOptions).populate(winnerOptions).populate(sponsorOptions).populate(playerOptions).lean().exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
//   Tournament.aggregate([
//     {$match: {_id: mongoose.Types.ObjectId(id)}},
//     {$unwind:"$players"},
//     {$lookup: {
//       from: 'users', 
//       localField: 'players.playerId', 
//       foreignField: '_id', 
//       as: 'userInfo'
//     }},
//     { "$group": {
//     "_id": "$_id",
//     "name" : { "$first": "$name" },
//     "entry_fee" : { "$first": "$entry_fee" },
//     "startdate" : { "$first": "$startdate" },
//     "enddate" : { "$first": "$enddate" },
//     "gameid" : { "$first": "$gameid" },
//     "manual" : { "$first": "$manual" },
//     "description" : { "$first": "$description" },
//     "logo" : { "$first": "$logo" },
//     "winners" : { "$first": "$winners" },
//     "sponsorid" : { "$first": "$sponsorid" },
//     "status" : { "$first": "$status" },
//     "players": { "$push": "$userInfo" }
//   }}
// ]).exec(function(err, data){
//     if(err)
//     {
//       callback(err);
//     }
//     else
//     {
//       callback(null, data);
//     }
//   });
}

module.exports.getTournamentById = function(id, callback){
  var gameOptions = {
      path: 'gameid',
      select: " name logo description genre _id",
  };
  var sponsorOptions = {
      path: 'sponsorid',
      select: " name logo description banner_ads_1 banner_ads_2 _id",
  };
  var playerOptions = {
        path: 'players.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  var winnerOptions = {
        path: 'winners.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  Tournament.findOne({_id: mongoose.Types.ObjectId(id)}).populate(gameOptions).populate(winnerOptions).populate(sponsorOptions).populate(playerOptions).lean().exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}

module.exports.getTournamentDetailById = function(id, callback){

  Tournament.aggregate([
  { "$match": { "_id": mongoose.Types.ObjectId(id) } },
  { "$lookup": {
    "from": "games",    
    "localField": "gameid",
    "foreignField": "_id",
    "as": "gameinfo"
  } },
  { "$unwind": "$gameinfo" },
  // { "$lookup": {
  //   "from": "sponsors",    
  //   "localField": "sponsorid",
  //   "foreignField": "_id",
  //   "as": "sponsorsinfo"
  // } },
  // { "$unwind": "sponsorsinfo" },
  { "$project": {
    "name": 1,
    "entry_fee": 1,
    "startdate": 1,
    "enddate": 1,
    "description": 1,
    "status": 1,
    "manual": 1,
    "sponsorid": 1,
    "gameinfo": 1,
    // "sponsorsinfo": 1,
  } }
]).exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data[0]);
    }
  });
}

module.exports.getTournamentByName = function(name, callback){

  Tournament.findOne({'name': name}).lean().exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}

module.exports.getActiveTournaments = function(condData={}, callback){
  let tmpCondData = {};
  tmpCondData = condData;
  tmpCondData.status = 'Ongoing';
  var gameOptions = {
      path: 'gameid',
      select: " name logo description genre _id",
  };
  var sponsorOptions = {
      path: 'sponsorid',
      select: " name logo description banner_ads_1 banner_ads_2 _id",
  };
  var playerOptions = {
        path: 'players.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
    var winnerOptions = {
        path: 'winners.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  Tournament.find(tmpCondData).populate(gameOptions).populate(sponsorOptions).populate(winnerOptions).populate(playerOptions).sort('startdate').lean().exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}

module.exports.getTournamentByPlayerId = function(playerid, callback){
  var gameOptions = {
      path: 'gameid',
      select: " name logo description genre _id",
  };
  var sponsorOptions = {
      path: 'sponsorid',
      select: " name logo description banner_ads_1 banner_ads_2 _id",
  };
  var playerOptions = {
        path: 'players.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
    var winnerOptions = {
        path: 'winners.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  Tournament.find({"name": { "$nin": [ null, "" ]}, 'players.playerId' : mongoose.Types.ObjectId(playerid)})
  .populate(gameOptions).populate(sponsorOptions).populate(winnerOptions).populate(playerOptions).sort('startdate').lean().exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
  // let tmpCondData = {'players.playerId' : mongoose.Types.ObjectId(playerid)};
  // Tournament.aggregate([
  //           { 
  //             "$match": tmpCondData
  //         },
  //         {$sort : { 'enddate' : -1}}, 
  //          {$lookup: {
  //           from: 'games', 
  //           localField: 'gameid', 
  //           foreignField: '_id', 
  //           as: 'gameInfo'
  //         }},
  //         {$lookup: {
  //           from: 'users', 
  //           localField: 'players.playerId', 
  //           foreignField: '_id', 
  //           as: 'playerInfo'
  //         }},
  //           { 
  //             $group: {
  //               _id: "$_id",
  //               entry_fee:{ $first: "$entry_fee" },
  //               startdate:{ $first: "$startdate"},
  //               enddate:{ $first: "$enddate"},
  //               name:{ $first: "$name"},
  //               gameid: {$first: "$gameid"},
  //               manual: {$first: "$manual"},
  //               status: {$first: "$status"},
  //               description: {$first: "$description"},
  //               sponsorid: {$first: "$sponsorid"},
  //               prizes: {$first: "$prizes"},
  //               players: {$first: "$players"},
  //               winners: {$first: "$winners"},
  //               gameName:{$first:"$gameInfo.name"},
  //               playerInfo:{$first:"$playerInfo"}
  //           } 
  //         }
  //         ])
  //         .exec(function(err, tournaments){
  //           if(err)
  //           { 
  //             callback(err);
  //           }
  //           else
  //           {
  //             callback(null, tournaments);
  //           }         
  //     });
}

module.exports.getWonTournamentByUserId = function(playerid, callback){
  var gameOptions = {
      path: 'gameid',
      select: " name logo description genre _id",
  };
  var sponsorOptions = {
      path: 'sponsorid',
      select: " name logo description banner_ads_1 banner_ads_2 _id",
  };
  var playerOptions = {
        path: 'players.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  var winnerOptions = {
        path: 'winners.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  Tournament.find({'winners.playerId' : mongoose.Types.ObjectId(playerid)}).populate(gameOptions).populate(sponsorOptions).populate(winnerOptions).populate(playerOptions).sort('startdate').lean().exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}
module.exports.getTournamentByGameId = function(gameid, callback){
  var gameOptions = {
      path: 'gameid',
      select: " name logo description genre _id",
  };
  var sponsorOptions = {
      path: 'sponsorid',
      select: " name logo description banner_ads_1 banner_ads_2 _id",
  };
  var playerOptions = {
        path: 'players.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  var winnerOptions = {
        path: 'winners.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  Tournament.find({"name": { "$nin": [ null, "" ]},'gameid' : mongoose.Types.ObjectId(gameid)}).populate(gameOptions).populate(sponsorOptions).populate(winnerOptions).populate(playerOptions).sort('startdate').lean().exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
  // let tmpCondData = {"name": { "$nin": [ null, "" ]}, 'gameid' : mongoose.Types.ObjectId(gameid)};
  // Tournament.aggregate([
  //           { 
  //             "$match": tmpCondData
  //         },
  //         {$sort : { 'enddate' : -1}}, 
  //          {$lookup: {
  //           from: 'games', 
  //           localField: 'gameid', 
  //           foreignField: '_id', 
  //           as: 'gameInfo'
  //         }},
  //         {$lookup: {
  //           from: 'users', 
  //           localField: 'players.playerId', 
  //           foreignField: '_id', 
  //           as: 'playerInfo'
  //         }},
  //           { 
  //             $group: {
  //               _id: "$_id",
  //               entry_fee:{ $first: "$entry_fee" },
  //               startdate:{ $first: "$startdate"},
  //               enddate:{ $first: "$enddate"},
  //               name:{ $first: "$name"},
  //               gameid: {$first: "$gameid"},
  //               manual: {$first: "$manual"},
  //               status: {$first: "$status"},
  //               description: {$first: "$description"},
  //               sponsorid: {$first: "$sponsorid"},
  //               prizes: {$first: "$prizes"},
  //               players: {$first: "$players"},
  //               winners: {$first: "$winners"},
  //               gameName:{$first:"$gameInfo.name"},
  //               playerInfo:{$first:"$playerInfo"}
  //           } 
  //         }
  //         ])
  //         .exec(function(err, tournaments){
  //           if(err)
  //           { 
  //             callback(err);
  //           }
  //           else
  //           {
  //             callback(null, tournaments);
  //           }         
  //     });
}

module.exports.getTournamentsbyIdAndPlayerId = function(tournamentid, playerid, callback){

  Tournament.find({'_id' : tournamentid, 'players.playerId' : mongoose.Types.ObjectId(playerid)}).sort('startdate').lean().exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}

module.exports.removeTournamentById = function(id, callback){
  Tournament.updateOne({'_id' : id}, {'status' : 'Deleted'}).exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}

module.exports.enrollPlayer = function(_tournamentId, rawData, callback){
  Tournament.updateOne({'_id': _tournamentId}, rawData, {upsert: true}).exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}

module.exports.updateTournament = function(id, jsondata, callback){
  if(id)
  {
    
    Tournament.findOneAndUpdate({ _id: id},{$set : jsondata}, {new: true})
    .exec(function(err, data){
       if(err){
          callback(err);
        }else{
          callback(null, data);
        } 
    });
  }
}

module.exports.getTournamentList = function(page, perPage, condData = {},callback){
  Tournament.find(condData)
  .sort('-created_on')
  .skip((perPage * page) - perPage)
  .limit(perPage)
  .exec(function(err, tournaments) {
    Tournament.countDocuments(condData).exec(function(err, count) {
      if(err)
      { 
        callback(err);
      }
      else
      {
        callback(null, {
          tournaments: tournaments,
          current: page,
          pages: Math.ceil(count / perPage)
        });
      }
    })
  })
}

module.exports.updateTournamentByQuery = function(myquery, jsondata, callback){
  Tournament.updateMany(myquery,{$set : jsondata})
  .exec(function(err, data){
     if(err){
        callback(err);
      }else{
        callback(null, data);
      } 
  });
}

module.exports.getTournamentPlayed = function(condData = {}, callback){
  Tournament.countDocuments(condData).exec(function(err, count) {
    if(err)
    { 
      callback(err);
    }
    else
    {
      callback(null, {
        count: count
      });
    }
  })
}

module.exports.getTournamentByStatus = function(condData = {}, pageData = {}, callback){
  let page = (pageData.page) ? pageData.page : 1;
  let perPage = (pageData.perPage) ? pageData.perPage : 100;
 
  var gameOptions = {
      path: 'gameid',
      select: " name logo description genre _id",
  };
  var sponsorOptions = {
      path: 'sponsorid',
      select: " name logo description banner_ads_1 banner_ads_2 _id",
  };
  var playerOptions = {
        path: 'players.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  var winnerOptions = {
        path: 'winners.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  Tournament.find(condData).populate(gameOptions).populate(sponsorOptions).populate(winnerOptions).populate(playerOptions).sort({'enddate': -1}).skip((perPage * page) - perPage).limit(perPage).lean().exec(function(err, tournaments){
    if(err)
    {
      callback(err);
    }
    else
    {
      Tournament.estimatedDocumentCount(condData).exec(function(err, count) {
          if(err)
          { 
            callback(err);
          }
          else
          {
            callback(null, {
              tournaments: tournaments,
              current: page,
              pages: Math.ceil(count / perPage)
            });
          }
      })
    }
  })
  // let page = (pageData.page) ? pageData.page : 1;
  // let perPage = (pageData.perPage) ? pageData.perPage : 100;
 
  // Tournament.aggregate([
  //           { 
  //             "$match": condData
  //         },
  //         {$sort : { 'enddate' : -1}}, 
  //          {$lookup: {
  //           from: 'games', 
  //           localField: 'gameid', 
  //           foreignField: '_id', 
  //           as: 'gameInfo'
  //         }},
  //         {$lookup: {
  //           from: 'users', 
  //           localField: 'players.playerId', 
  //           foreignField: '_id', 
  //           as: 'playerInfo'
  //         }},
  //           { 
  //             $group: {
  //               _id: "$_id",
  //               entry_fee:{ $first: "$entry_fee" },
  //               startdate:{ $first: "$startdate"},
  //               enddate:{ $first: "$enddate"},
  //               name:{ $first: "$name"},
  //               gameid: {$first: "$gameid"},
  //               manual: {$first: "$manual"},
  //               status: {$first: "$status"},
  //               description: {$first: "$description"},
  //               sponsorid: {$first: "$sponsorid"},
  //               prizes: {$first: "$prizes"},
  //               players: {$first: "$players"},
  //               winners: {$first: "$winners"},
  //               gameName:{$first:"$gameInfo.name"},
  //               playerInfo:{$first:"$playerInfo"}
  //           } 
  //         }
  //         ])
  //         .skip((perPage * page) - perPage)
  //         .limit(perPage)
  //         .exec(function(err, tournaments){
  //           Tournament.countDocuments(condData).exec(function(err, count) {
  //             if(err)
  //             { 
  //               callback(err);
  //             }
  //             else
  //             {
  //               callback(null, {
  //                 tournaments: tournaments,
  //                 current: page,
  //                 pages: Math.ceil(count / perPage)
  //               });
  //             }
  //           })          
  //     });

}