var mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
mongoose.set('debug', true);


var betSchema  = mongoose.Schema({
  walletId        : { type : String,  required : true },
  playerId        : { type : mongoose.Schema.ObjectId, ref: 'user', required : true },
  tournamentId    : { type : mongoose.Schema.ObjectId, ref: 'tournament', default : null },
  prediction      : { type : String },
  luckyNumber     : { type : Number, default : 0},
  betAmount       : { type : Number, default: 0 },
  payoutAmount    : { type : Number, default: 0 },
  result          : { type : String, enum : ['win', 'loose']},
  created_on      : { type : Date },
  updated_on      : { type : Date }

});

var Bet = module.exports = mongoose.model('Bet', betSchema); 

module.exports.saveDetails = function(betData, callback){
  betData.save(function(err){
    if(err){
      callback(err);
    }else {
      callback(null, 'success');
    }
  })
}

module.exports.getBetData = function( page, perPage, condData = {}, callback){
      query = Bet.find(condData).skip((perPage * page) - perPage).limit(perPage);
      query.exec(function(err, bets){        
            if(err){
              callback(err);
            }else{
              Bet.estimatedDocumentCount().exec(function(err, count) {
                  if(err)
                  { 
                    callback(err);
                  }
                  else
                  {
                    callback(null, {
                      bets: bets,
                      current: page,
                      pages: Math.ceil(count / perPage)
                    });
                  }
              })
              //callback(null, data);
            }           
      });
}


module.exports.getBetsByTournamentId = function(tournamentid, callback){
  Bet.find({'tournamentId' : mongoose.Types.ObjectId(tournamentid)}).sort('-betAmount').lean().exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}

module.exports.getBetsCount = function(callback){
  Bet.countDocuments({'tournamentId' : { $ne : null }}).lean().exec(function(err, betsWithTournament){
    if(err)
    {
      callback(err);
    }
    else
    {
      Bet.countDocuments({'tournamentId' : null}).lean().exec(function(err, betsWithOutTournament){
         if(err)
        {
          callback(err);
        }
        else
        { 
          var response = {};
          response.betsWithTournament = betsWithTournament;
          response.betsWithOutTournament = betsWithOutTournament;
          callback(null, response);
        }
      })
    }
  })
}

module.exports.getNormalBetTotalPayout = function(callback){  
    Bet.aggregate([
            { 
              "$match": { 
                 "$and":[
              {
                tournamentId : null
              },
              {
                result : 'win'
              }
             ]
            } 
          },
            { 
              $group: {
               _id: null,
                payoutAmount: 
                { $sum: "$payoutAmount" },
                count: { $sum: 1 }
            } 
          }
          ]).exec(function(err, payputs){
            if(err){
              callback(err, null);
            }else if(payputs.length> 0){
              callback(null, {betPayout: payputs[0].payoutAmount});
            }else{
              callback(null, {betPayout : 0});
            }           
      });
}

module.exports.getTournamentBetTotalPayout = function(callback){  
    Bet.aggregate([
            { 
              "$match": { 
                 "$and":[
              {
                tournamentId : { $ne : null }
              },
              {
                result : 'win'
              }
             ]
            } 
          },
            { 
              $group: {
               _id: null,
                payoutAmount: 
                { $sum: "$payoutAmount" },
                count: { $sum: 1 }
            } 
          }
          ]).exec(function(err, payputs){
            if(err){
              callback(err, null);
            }else if(payputs.length> 0){
              callback(null, {tournamentBetPayout: payputs[0].payoutAmount});
            }else{
              callback(null, {tournamentBetPayout : 0});
            }           
      });
}

module.exports.getBetsCountByPlayer = function(tournamentid, playerid, callback){
  Bet.countDocuments({'tournamentId' : mongoose.Types.ObjectId(tournamentid), 'playerId' : mongoose.Types.ObjectId(playerid)}).lean().exec(function(err, betsCount){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, betsCount);
    }
  })
}