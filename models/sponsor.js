var mongoose = require('mongoose');
// mongoose.set('debug', true);

var SponsorSchema  = mongoose.Schema({
	name 			: { type : String, required : true, unique : true },
	logo 		    : { type : String}, 
	banner_ads_1 	: { type : String},
	banner_ads_2 	: { type : String}, 
	banner_ads_3 	: { type : String}, 
	description 	: { type : String },
	status 			: { type : String, enum : ['active', 'inactive'], default : 'active' },
	created_on 		: { type : Date, default : Date.now()}
})

var Sponsor = module.exports = mongoose.model('Sponsor', SponsorSchema); 

module.exports.getAllSponsors = function(condData = {}, fields= {}, callback){
  Sponsor.find(condData, fields)
  .sort('-created_on')
  .exec(function(err, sponsors) {
    callback(null, {
      sponsors: sponsors,
    });
  })
}

module.exports.createData = function(SponsorData, callback){
	Sponsor.create(SponsorData, function(err, sponsor){
    if(err){
			callback(err);
		}
		else
		{
			callback(null, sponsor);
		}
	})
}

module.exports.getSponsorByName = function(name, callback){

  Sponsor.findOne({'name' : name}).lean().exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}

module.exports.getSponsorById = function(id, callback){

	Sponsor.findOne({'_id' : id}).lean().exec(function(err, data){
		if(err)
		{
			callback(err);
		}
		else
		{
			callback(null, data);
		}
	})
}

module.exports.getActiveSponsorList = function(callback){

	Sponsor.find({'status' : 'active'}).sort('-created_on').lean().exec(function(err, data){
		if(err)
		{
			callback(err);
		}
		else
		{
			callback(null, data);
		}
	})
}

module.exports.getSponsorList = function(page, perPage, condData = {},callback){
  Sponsor.find(condData)
  .sort('-created_on')
  .skip((perPage * page) - perPage)
  .limit(perPage)
  .exec(function(err, sponsors) {
    Sponsor.countDocuments(condData).exec(function(err, count) {
      if(err)
      { 
        callback(err);
      }
      else
      {
        callback(null, {
          sponsors: sponsors,
          current: page,
          pages: Math.ceil(count / perPage)
        });
      }
    })
  })
}
 
module.exports.removeSponsorById = function(id, callback){
	Sponsor.updateOne({'_id' : id}, {'status' : 'Deleted'}).exec(function(err, data){
		if(err)
		{
			callback(err);
		}
		else
		{
			callback(null, data);
		}
	})
}

module.exports.updateSponsor = function(id, jsondata, callback){
  if(id)
  {
  	
   	Sponsor.findOneAndUpdate({ _id: id},{$set : jsondata}, {new: true})
   	.exec(function(err, data){
    	 if(err){
          callback(err);
        }else{
          callback(null, data);
        } 
    });
  }
}