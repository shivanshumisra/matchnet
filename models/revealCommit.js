var mongoose = require('mongoose');
// mongoose.set('debug', true);

var RevealCommitSchema  = mongoose.Schema({
	reveal 			: { type : String, required : true, unique : true },
	commit 		  : { type : String, required : true, unique : true },
  commitHash      : { type : String, default:""}, 
  revealHash      : { type : String, default:"" },  
	created_on 		: { type : Date, default : Date.now()}
})

var RevealCommit = module.exports = mongoose.model('RevealCommit', RevealCommitSchema); 


module.exports.getRevealCommits = function(commit, callback){
  RevealCommit.findOne({commit : commit})
  .exec(function(err, data) {
    callback(null, data);
  })
}

module.exports.createData = function(RevealCommitData, callback){
	RevealCommit.create(RevealCommitData, function(err, RevealCommit){
    if(err){
			callback(err);
		}
		else
		{
			callback(null, RevealCommit);
		}
	})
}
