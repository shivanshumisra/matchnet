var mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
/*mongoose.set('debug', true);
*/
let dividend = mongoose.Schema({
  dailyDividend : {type: Number, default: 0},
  created_on : {type: Date, default: new Date()}
})

let matchToken = mongoose.Schema({
  canWithdraw : {type: Number, default: 0},
  canFreeze : {type: Number, default: 0},
  totalFrozen : {type: Number, default: 0}
})

var userSchema  = mongoose.Schema({
  username        : { type : String, default : ''},
  password        : { type : String, default:'' },
  fname           : { type : String,  default : '' },
  lname           : { type : String,  default : '' },
  email           : { type : String, default : ''},
  age             : { type : String, default:'' },
  gender          : { type : String, enum : ['male', 'female', 'other', ''], default : '' },
  walletid        : { type : String, default:'' },
  ImageUrl        : { type : String, default:'' },
  is_verified       : { type : String, enum : ['no', 'yes'], default : 'no' },
  status            : { type : String, enum : ['active', 'inactive'], default : 'inactive' },
  user_type         : { type : String, default:'' },
  is_deleted        : {type: Number, default: 0},
  userDividends     : [dividend],
  match_tokens      : matchToken,
  universal_level : {type: Number, default: 0},
  individual_game_level : {type: Number, default: 0},
  created_on        : { type : Date , default: new Date()},
  updated_on        : { type : Date , default: new Date()},
  total_win_amount  : {type : Number, default: 0}

});

var User = module.exports = mongoose.model('user', userSchema); 

module.exports.saveDetails = function(userData, callback){
  userData.save(function(err){
    if(err){
      callback(err);
    }
    else
    {
      callback(null, 'success');
    }
  })
}

module.exports.createUser = function(userData, callback){
  User.create(userData, function (error, user) {
    if (error) {
      callback(error);
    } else {
      callback(null, user);
    }
  })
}

module.exports.getUserData = function(page, perPage, condData = {}, callback){
  User.find(condData)
  .skip((perPage * page) - perPage)
  .sort('-created_on')
  .limit(perPage)
  .exec(function(err, users) {
    User.countDocuments(condData).exec(function(err, count) {
      if(err)
      { 
        callback(err);
      }
      else
      {
        callback(null, {
          users: users,
          current: page,
          pages: Math.ceil(count / perPage)
        });
      }
    })
  })
}

module.exports.getAllUserData = function(condData = {}, callback){
  User.find(condData, {'walletid': true,'email': true,'status': true,'is_verified': true,})
  .sort('-created_on')
  .exec(function(err, users) {
    callback(null, {
      users: users,
    });
  })
}

module.exports.getUserByUsername = function(username, callback){
      query = User.findOne({ 'username' :username });
      query.exec(function(err, data){        
            if(err){
              callback(err);
            }else{
              callback(null, data);
            }           
      });
}

module.exports.getUserByWalletId = function(walletid, callback){
      query = User.findOne({ 'walletid' :walletid });
      query.exec(function(err, data){        
            if(err){
              callback(err);
            }else{
              callback(null, data);
            }           
      });
}

module.exports.getUserById = function(id, callback){
      query = User.findOne({ '_id' :id });
      query.exec(function(err, data){        
            if(err){
              callback(err);
            }else{
              callback(null, data);
            }           
      });
}


module.exports.update_commonById = function(id, json, callback){
  if(id && json)
  {
    User.findOneAndUpdate({ _id: id},{$set : json}, {new: true})
    .exec(function(err, data){
       if(err){
          callback(err);
        }else{
          callback(null, data);
        } 
    });
  }
}