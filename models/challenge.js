var mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
//mongoose.set('debug', true);

var selectedSlot = mongoose.Schema({
    playerId : {type: mongoose.Schema.ObjectId, ref: 'user', required : true},
    walletId : { type : String},
    slot : { type : String}
  });

var challengeSchema  = mongoose.Schema({
  title             : { type : String, default: ''},
  noOfPlayer        : { type : Number, default : 0},
  remainingPlayer   : { type : Number, default : 0},
  challengeFee      : {type : Number, default : 0 },
  selectedSlot      : [selectedSlot],
  creator           : {type: mongoose.Schema.ObjectId, ref: 'user', required : true},
  winAmount         : { type : Number, default : 0},
  slots             : [{ type : String, required : true}],
  slotOccupied      : [{ type : String}],
  winner            : {type: mongoose.Schema.ObjectId, ref: 'user', default:null},
  luckyNumber       :  { type : Number, default : 0.1},
  status            : { type : String, enum : ['open', 'start', 'close'], default : 'open' },
  created_on        : { type : Date },
  updated_on        : { type : Date }
});

var Challenge = module.exports = mongoose.model('Challenge', challengeSchema); 

var userOptions = {
        path: 'selectedSlot.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
module.exports.getAllChallenges = function(condData = {}, fields= {}, callback){
  Challenge.find(condData, fields).populate(userOptions)
  .sort('-created_on')
  .exec(function(err, challenges) {
    callback(null, {
      challenges: challenges,
    });
  })
}

module.exports.saveDetails = function(challengeData, callback){
  challengeData.save(function(err){
    if(err){
      callback(err);
    }else {
      callback(null, 'success');
    }
  })
}

module.exports.getChallengePlayed = function(callback){
  Challenge.countDocuments({"status" : "close"}).exec(function(err, count) {
    if(err)
    { 
      callback(err);
    }
    else
    {
      callback(null, {
        challengeCount: count
      });
    }
  })
}

module.exports.getChallengeById = function(ChallengeId, callback){
  var creatorOptions = {
        path: 'creator',
        select: " fname lname email age walletid ImageUrl _id",
    };
    var winnerOptions = {
      path: 'winner',
      select: " fname lname email age walletid ImageUrl _id",
  };
  query = Challenge.findOne({_id: ChallengeId}).populate(creatorOptions).populate(winnerOptions);
   query.exec(function(err, data){        
            if(err){
              callback(err);
            }else{
              callback(null, data);
            }           
      });
}
module.exports.getChallengeBycreator = function(playerId, callback){
   var winnerOptions = {
      path: 'winner',
      select: " fname lname email age walletid ImageUrl _id",
  };
  var creatorOptions = {
        path: 'creator',
        select: " fname lname email age walletid ImageUrl _id",
    };
 var userOptions = {
        path: 'selectedSlot.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  query = Challenge.find({"creator": playerId}).populate(userOptions).populate(creatorOptions).populate(winnerOptions);
   query.exec(function(err, data){        
            if(err){
              callback(err);
            }else{
              callback(null, data);
            }           
      });
}


module.exports.getChallengeByPlayerId = function(playerId, callback){
  var userOptions = {
        path: 'selectedSlot.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  query = Challenge.find({"$and":[{"selectedSlot.playerId": playerId}, {"creator": { "$ne" : playerId }}]}).populate(userOptions);
   query.exec(function(err, data){        
            if(err){
              callback(err);
            }else{
              callback(null, data);
            }           
      });
}

module.exports.getChallengeByStatus = function(status, callback){
  var creatorOptions = {
        path: 'creator',
        select: " fname lname email age walletid ImageUrl _id",
    };
    var userOptions = {
        path: 'selectedSlot.playerId',
        select: " fname lname email age walletid ImageUrl _id",
    };
  query = Challenge.find({status: status}).populate(creatorOptions).populate(userOptions);
   query.exec(function(err, data){        
            if(err){
              callback(err);
            }else{
              callback(null, data);
            }           
      });
}

module.exports.getChallengeData = function( page, perPage, condData = {}, callback){
  var creatorOptions = {
        path: 'creator',
        select: " fname lname email age walletid ImageUrl _id",
    };
  var winnerOptions = {
      path: 'winner',
      select: " fname lname email age walletid username ImageUrl _id",
  };
  var playerOptions = {
        path: 'selectedSlot.playerId',
        select: " fname lname email age walletid username ImageUrl _id",
    };
      query = Challenge.find(condData).populate(creatorOptions).populate(winnerOptions).populate(playerOptions).sort('-created_on').skip((perPage * page) - perPage).limit(perPage);
      query.exec(function(err, challenge){        
            if(err){
              callback(err);
            }else{
              Challenge.estimatedDocumentCount(condData).exec(function(err, count) {
                  if(err)
                  { 
                    callback(err);
                  }
                  else
                  {
                    callback(null, {
                      challenge: challenge,
                      current: page,
                      pages: Math.ceil(count / perPage)
                    });
                  }
              })
            }           
      });

      // Challenge.aggregate([
      //       { 
      //         "$match": condData
      //     },
      //      {$lookup: {
      //       from: 'users', 
      //       localField: 'winner', 
      //       foreignField: '_id', 
      //       as: 'winnerInfo'
      //     }},
      //     {$lookup: {
      //       from: 'users', 
      //       localField: 'selectedSlot.playerId', 
      //       foreignField: '_id', 
      //       as: 'playerInfo'
      //     }},
      //       { 
      //         $group: {
      //           _id: "$_id",
      //           title:{ $first: "$title" },
      //           noOfPlayer:{ $first: "$noOfPlayer"},
      //           remainingPlayer:{ $first: "$remainingPlayer"},
      //           challengeFee:{ $first: "$challengeFee"},
      //           winAmount:{ $first: "$winAmount"},
      //           winner:{ $first: "$winner"},
      //           slots: {$first: "$slots"},
      //           slotOccupied: {$first: "$slotOccupied"},
      //           status: {$first: "$status"},
      //           luckyNumber: {$first: "$luckyNumber"},
      //           creator: {$first: "$creator"},
      //           selectedSlot: {$first: "$selectedSlot"},
      //           created_on: {$first: "$created_on"},
      //           updated_on: {$first: "$updated_on"},
      //           winnerInfo:{$first:"$winnerInfo"},
      //           playerInfo:{$first:"$playerInfo"}
      //       } 
      //     }
      //     ])
      //     .skip((perPage * page) - perPage)
      //     .limit(perPage)
      //     .exec(function(err, challenge){
      //       Challenge.countDocuments(condData).exec(function(err, count) {
      //         if(err)
      //         { 
      //           callback(err);
      //         }
      //         else
      //         {
      //           callback(null, {
      //             challenge: challenge,
      //             current: page,
      //             pages: Math.ceil(count / perPage)
      //           });
      //         }
      //       })          
      // });
}

module.exports.updateChallenge = function(id, slot, status, jsondata, callback){

  query = {'_id': id, status:"open"},
  update = {
    $set: {status : status},
    $push: {selectedSlot: jsondata, slotOccupied : slot},
    $inc:{remainingPlayer : -1}
  };
    Challenge.findOneAndUpdate(query,update, {new: true})
    .exec(function(err, data){
       if(err){
          callback(err);
        }else{
          callback(null, data);
        } 
    });
}

module.exports.updateWinner = function(id, winner, luckyNumber, status, callback){

  var creatorOptions = {
      path: 'creator',
      select: " fname lname email age walletid ImageUrl _id",
  };
  var winnerOptions = {
      path: 'winner',
      select: " fname lname email age walletid ImageUrl _id",
  };

  query = {'_id': id, status:"start"},
  update = {
      $set: {winner: mongoose.Types.ObjectId(winner), status : status, luckyNumber : luckyNumber}
  };
    Challenge.findOneAndUpdate(query,update, {new: true}).populate(creatorOptions).populate(winnerOptions)
    .exec(function(err, data){
       if(err){
          callback(err);
        }else{
          callback(null, data);
        } 
    });
}

module.exports.getChallengeWinAmount = function(callback){
  Challenge.aggregate([
            { 
              "$match": {
                  "$and":[
                  {
                    winner : {$ne : null}
                  },
                  {
                    status : "close"
                  }
                ] 
            } 
          },
            { 
              $group: {
               _id: null,
                winAmount: 
                { $sum: "$winAmount" },
                count: { $sum: 1 }
            } 
          }
          ]).exec(function(err, payputs){
            if(err){
              callback(err, null);
            }else if(payputs.length> 0){
              callback(null, {challengePayout: payputs[0].winAmount});
            }else{
              callback(null, {challengePayout : 0});
            }           
      });
}