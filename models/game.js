var mongoose = require('mongoose');
// mongoose.set('debug', true);

var GameSchema  = mongoose.Schema({
	name 			: { type : String, required : true, unique : true },
	logo 		    : { type : String}, 
	description 	: { type : String },
	genre				: {type: String, enum: ["Luck", "Skill"], default: "Luck"},
	status 			: { type : String, enum : ['active', 'inactive'], default : 'active' },
	created_on 		: { type : Date, default : Date.now()}
})

var Game = module.exports = mongoose.model('Game', GameSchema); 


module.exports.getAllGames = function(condData = {}, fields= {}, callback){
  Game.find(condData, fields)
  .sort('-created_on')
  .exec(function(err, sponsors) {
    callback(null, {
      sponsors: sponsors,
    });
  })
}

module.exports.createData = function(GameData, callback){
	Game.create(GameData, function(err, game){
    if(err){
			callback(err);
		}
		else
		{
			callback(null, game);
		}
	})
}

module.exports.getGameByName = function(name, callback){

  Game.findOne({'name' : name}).lean().exec(function(err, data){
    if(err)
    {
      callback(err);
    }
    else
    {
      callback(null, data);
    }
  })
}

module.exports.getGameById = function(id, callback){

	Game.findOne({'_id' : id}).lean().exec(function(err, data){
		if(err)
		{
			callback(err);
		}
		else
		{
			callback(null, data);
		}
	})
}

module.exports.getGameList = function(page, perPage, condData = {},callback){
  Game.find(condData)
  .sort('-created_on')
  .skip((perPage * page) - perPage)
  .limit(perPage)
  .exec(function(err, games) {
    Game.countDocuments(condData).exec(function(err, count) {
      if(err)
      { 
        callback(err);
      }
      else
      {
        callback(null, {
          games: games,
          current: page,
          pages: Math.ceil(count / perPage)
        });
      }
    })
  })
}

module.exports.removeGameById = function(id, callback){
	Game.updateOne({'_id' : id}, {'status' : 'Deleted'}).exec(function(err, data){
		if(err)
		{
			callback(err);
		}
		else
		{
			callback(null, data);
		}
	})
}

module.exports.updateGame = function(id, jsondata, callback){
  if(id)
  {
  	
   	Game.findOneAndUpdate({ _id: id},{$set : jsondata}, {new: true})
   	.exec(function(err, data){
    	 if(err){
          callback(err);
        }else{
          callback(null, data);
        } 
    });
  }
}
