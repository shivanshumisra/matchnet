var mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
//mongoose.set('debug', true);


var pointSchema  = mongoose.Schema({
  walletId        : { type : String,  required : true },
  tournamentId    : { type : mongoose.Schema.ObjectId, ref: 'tournament', required:true },
  playerId        : { type : mongoose.Schema.ObjectId, ref: 'user', required : true },
  points          : { type : Number, default : 0},
  result          : { type : String, enum : ['win', 'loose']},
  betamount        : { type : Number, default : 0},
  created_on      : { type : Date },
  updated_on      : { type : Date }
});

var Point = module.exports = mongoose.model('Point', pointSchema); 

module.exports.saveDetails = function(pointData, callback){
  pointData.save(function(err){
    if(err){
      callback(err);
    }else {
      callback(null, 'success');
    }
  })
}

module.exports.getPointData = function( playerId, tournamentId, callback){  
    Point.aggregate([
            { 
              "$match": { 
                 "$and":[
               {
                playerId : mongoose.Types.ObjectId(playerId)
              },
              {
                tournamentId : mongoose.Types.ObjectId(tournamentId)
              },
              {
                result : 'win'
              }
             ]
            } 
          },
            { 
              $group: {
               _id: null,
                betamount: 
                { $sum: "$betamount" 
              } 
            } 
          }
          ]).exec(function(err, points){
            if(err){
              callback(err, null);
            }else if(points.length > 0){
              callback(null, points);
            }else{
              callback(null, {data : null});
            }           
      });
}

module.exports.getPointByTournamentId = function(tournamentId, callback){  
    Point.aggregate([
            { 
              "$match": {
                tournamentId : mongoose.Types.ObjectId(tournamentId)
            } 
          },
          {$sort : { 'points' : -1}}, 
           {$lookup: {
            from: 'users', 
            localField: 'playerId', 
            foreignField: '_id', 
            as: 'userInfo'
          }},
            { 
              $group: {
                _id: "$playerId",
                betamount:{ $sum: "$betamount" },
                points:{ $first: "$points"},
                walletId:{ $first: "$walletId"},
                playerId:{ $first: "playerId"},
                result:{ $first: "$result"},
                userInfo: {$first: "$userInfo"}
            } 
          }
          ]).exec(function(err, points){
            if(err){
              callback(err, null);
            }else if(points.length > 0){
              callback(null, points);
            }else{
              callback(null, {data : null});
            }           
      });
}

module.exports.getLeaderBoard = function(tournamentId, callback){  
    Point.aggregate([
            { 
              "$match": {
                tournamentId : mongoose.Types.ObjectId(tournamentId)
              }
            },
            {$sort : { 'points' : -1}}, 
           {$lookup: {
            from: 'users', 
            localField: 'playerId', 
            foreignField: '_id', 
            as: 'userInfo'
          }},
            { 
              $group: {
                _id: "$playerId",
                betamount:{ $sum: "$betamount" },
                points:{ $first: "$points"},
                walletId:{ $first: "$walletId"},
                result:{ $first: "$result"},
                userInfo: {$first: "$userInfo"}
            } 
          },
          {$sort : { 'points' : -1}}, 
          ]).exec(function(err, points){
            if(err){
              callback(err, null);
            }else if(points.length > 0){
              callback(null, points);
            }else{
              callback(null, []);
            }           
      });
}


module.exports.getMaxPointDetailByTournamentId = function(tournamentId, callback){  
    Point.aggregate([
          {$sort : { 'points' : -1, 'betamount': -1}}, 
          { 
            "$match": {
              tournamentId : mongoose.Types.ObjectId(tournamentId),
              result: 'win'
            } 
          },
          
          { 
            $group: {
              _id: "$playerId",
              "maxScore": {
                "$max": "$points"
              },
              betamount:{ $sum: "$betamount" },
              points:{ $first: "$points"},
              walletId:{ $first: "$walletId"},
              playerId:{ $first: "$playerId"},
              result:{ $first: "$result"},
            } 
          },
          {$sort : { 'points' : -1, 'betamount': -1}},
          // { $limit : 1 }
          ]).exec(function(err, points){
            if(err){
              callback(err, null);
            }else if(points.length > 0){
              callback(null, points);
            }else{
              callback(null, {data : null});
            }           
      });
}