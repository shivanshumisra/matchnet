exports = module.exports = function(app, user, bet, challenge ,point, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, io, _){
  

/** Api to save bet response **/  
app.post('/saveBet', function(req, res){
  function saveBet(){
    return new Promise(function(resolve, reject){
      var userData = new bet({
        walletId: req.body.walletid,
        playerId: req.body.playerid,
        tournamentId: req.body.tournamentid ? req.body.tournamentid : null,
        prediction: req.body.prediction,
        luckyNumber: req.body.luckynumber,
        betAmount: req.body.betamount,
        payoutAmount: req.body.payoutamount,
        result: (req.body.result).toLowerCase(),
        created_on: MOMENT().format('YYYY-MM-DD HH:mm:ss'),
        updated_on: MOMENT().format('YYYY-MM-DD HH:mm:ss')
      });

      bet.saveDetails(userData, function(err, response){
        if(err){
          reject(new Error(err));
        }
        else{
            resolve(response);
          }
      })
    })
  }
  function getPointsInfo(){
    return new Promise((resolve, reject)=> {
      point.getPointData(req.body.playerid, req.body.tournamentid, (err, response)=>{
        if(err)
        {
          reject(err)
        }
        else
        {
          resolve(response)
        }
      })
    })
  }

  function getLeaderBoard(tournamentid){
    return new Promise(function(resolve, reject){
      point.getLeaderBoard(tournamentid, function(err, response){
        if(err){
          reject(null, new Error(err));
        }
        else{
          resolve(response);
        }
      })
    })
  }

  function savePoints(data){
    return new Promise((resolve, reject)=>{
      var pointData = new point({
        walletId: req.body.walletid,
        tournamentId : req.body.tournamentid,
        playerId: req.body.playerid,
        result: (req.body.result).toLowerCase(),
        betamount: req.body.betamount,
        points: (data == 'fresh') ? ((req.body.result).toLowerCase() == 'win' ? Math.floor(parseInt(_.get(req, 'body.betamount')) / 100) : 0) : ((req.body.result).toLowerCase() == 'win' ? Math.floor((parseInt(_.get(data[0], 'betamount')) + parseInt(_.get(req, 'body.betamount'))) / 100) : 0), 
        created_on: MOMENT().format('YYYY-MM-DD HH:mm:ss'),
        updated_on: MOMENT().format('YYYY-MM-DD HH:mm:ss')        
    });


    point.saveDetails(pointData, function(err, response){
      if(err){
        reject(new Error(err));
      }
      else{
          getLeaderBoard(req.body.tournamentid).then((respn)=>{
            global.io.emit("tournament_points", {details:respn, message: "tournament points"});
          })
          resolve(response);
        }
      })
    })
  }

  if(validator.isEmpty(_.get(req, 'body.walletid'))) {
    customValidationMsg('Wallet Id is required.', res);
  }else if(validator.isEmpty(_.get(req, 'body.playerid', ''))) {
    customValidationMsg('Player Id is required.', res);
  }else if(validator.isEmpty(_.get(req, 'body.prediction', ''))) {
    customValidationMsg('Prediction is required.', res);
  }else if(validator.isEmpty(_.get(req, 'body.betamount', ''))) {
    customValidationMsg('Bet Amount is required.', res);
  }else if(validator.isEmpty(_.get(req, 'body.payoutamount', ''))) {
    customValidationMsg('Payout Amount is required.', res);
  }else if(validator.isEmpty(_.get(req, 'body.result', ''))) {
    customValidationMsg('Status is required.', res);
  }
  else{
    saveBet()
    .then(function(response){

      if(validator.isEmpty(_.get(req, 'body.tournamentid')))
      {
        return response;
      }
      else
      {
        return getPointsInfo();
      }
       
    })
    .then((response)=>{
      if(validator.isEmpty(_.get(req, 'body.tournamentid')))
      {
         return (response);
      }
      else{

        if( !_.isArray(response) && _.get(response, 'data', null) == null)
        {
          return savePoints('fresh');
        }
        else
        {
          return savePoints(response);
        }
        
      }
        
    })
    .then((response)=>{
           var data = {detail: response, token: _.get(req, 'body.token', ''), status : true, message : 'Your bet is placed successfully.'};
            var json_arr  = {data: data, success: true};
            res.contentType('application/json');
            res.end(JSON.stringify(json_arr));
        })
        .catch(function(err){
            var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
            var json_arr  = {data: data, success: false};
            res.contentType('application/json');
            res.end(JSON.stringify(json_arr));
        })
      }
})

/** Api to get all home page stuff **/  
app.post('/getHomeStats', (req, res) =>{

  function getBetCount(){
    return new Promise((resolve, reject)=>{
         bet.getBetsCount((err, result)=>{
          if(err)
          {
            reject(err);
          }
          else{
            resolve(result);
          }
         })
    })
  }

  function getChallengeCount(){
    return new Promise((resolve, reject)=>{
         challenge.getChallengePlayed((err, result)=>{
          if(err)
          {
            reject(err);
          }
          else{
            resolve(result);
          }
         })
    })
  }

  function getNormalBetWinAmount(){
    return new Promise((resolve, reject)=>{
         bet.getNormalBetTotalPayout((err, result)=>{
          if(err)
          {
            reject(err);
          }
          else{
            resolve(result);
          }
       })
    })
  }

  function getTournamentBetWinAmount(){
    return new Promise((resolve, reject)=>{
         bet.getTournamentBetTotalPayout((err, result)=>{
          if(err)
          {
            reject(err);
          }
          else{
            resolve(result);
          }
         })
    })
  }

  function getChallengeWinAmount(){
    return new Promise((resolve, reject)=>{
         challenge.getChallengeWinAmount((err, result)=>{
          if(err)
          {
            reject(err);
          }
          else{
            resolve(result);
          }
         })
    })
  }
  var getBetsCount = getBetCount();
  var getChallengeCount = getChallengeCount();
  var getNormalBetWinAmount = getNormalBetWinAmount();
  var getTournamentBetWinAmount = getTournamentBetWinAmount();
  var getChallengeWinAmount = getChallengeWinAmount();

  Promise.all([getBetsCount, getChallengeCount , getNormalBetWinAmount, getTournamentBetWinAmount, getChallengeWinAmount
   ])
    .then((response)=>{
      var data = {detail: {
        betsWithTournament :response[0].betsWithTournament, 
        betsWithOutTournament :response[0].betsWithOutTournament, 
        challengeBetCount :response[1].challengeCount,
        totalBets : (response[0].betsWithTournament + response[0].betsWithOutTournament + response[1].challengeCount),
        betPayout :response[2].betPayout,
        tournamentBetPayout :response[3].tournamentBetPayout,
        challengePayout : response[4].challengePayout,
        totalPayout : (response[2].betPayout + response[3].tournamentBetPayout + response[4].challengePayout),
      }, status : true, };
      res.contentType('application/json');
      res.end(JSON.stringify(data));
    })
    .catch((err)=>{
      var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
      var json_arr  = {data: data, success: false};
      res.contentType('application/json');
      res.end(JSON.stringify(json_arr));
    })
  })
}