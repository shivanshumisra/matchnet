exports = module.exports = function(app, io, user, bet, point, challenge, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, _, TronWeb){
  
app.post('/api/getChallenges', function(req, res){
  function getChallengeData(){
    let condData = {};
    let fields = {'title': true,'status': true,'challengeFee': true, 'noOfPlayer':true, 'winAmount': true};
    return new Promise(function(resolve, reject){
      challenge.getAllChallenges(condData, fields, function(err, response){
        if(err)
        {
          reject(null, new Error(err));
        }
        else
        {
          resolve(response);
        }
      })
    })
  }
  getChallengeData()
  .then(function(response){
    var json_arr  = {data: ((response.challenges) ? response.challenges : []), success: true, message : 'challenges fetched successfully'};
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
  })
  .catch(function(derr, err){
    var resdata = {status : 0, error: err , pages:''};
    var json_arr  = {challenges: resdata, success: false, message : 'Oops!! An error occured while updating details.Please try again'};
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
  }) 
})
/** Api to save challenge  **/  

 app.post('/saveChallenge', function(req, res){

    function saveChallenge(){
      return new Promise(function(resolve, reject){

        var newObj = {};
        var selectedSlot = [];
        newObj.playerId = req.body.playerId;
        newObj.walletId = req.body.walletId;
        newObj.slot = req.body.selected_slot;
        selectedSlot.push(newObj);

        var challengeData = new challenge({
          title     : req.body.title ? req.body.title : '',
          noOfPlayer: req.body.no_of_player,
          remainingPlayer : req.body.no_of_player - 1,
          creator : req.body.playerId,
          challengeFee: req.body.challenge_fee,
          selectedSlot: selectedSlot,
          winAmount: req.body.win_amount,
          slots: req.body.slots,
          slotOccupied : [req.body.selected_slot],
          created_on: MOMENT().format('YYYY-MM-DD HH:mm:ss'),
          updated_on: MOMENT().format('YYYY-MM-DD HH:mm:ss')
        });

        challenge.saveDetails(challengeData, function(err, response){
          if(err){
            reject(new Error(err));
          }
          else{
              resolve(challengeData);
            }
        })
      })
    }

     function getChallengeInfo(resp){

      return new Promise((resolve, reject)=>{
        challenge.getChallengeById(resp._id, (err, data)=>{
          if(err)
          {
            reject(err)
          }else
          {
            resolve(data)
          }
        })
      })
    }
    function emitSocket(response){
      global.io.emit('challenge_published' ,{data: response, message : (_.get(req, 'body.playerName') ?  _.get(req, 'body.playerName') : _.get(req, 'body.walletId')) +" has published a challenge " + _.get(req, 'body.title') });
    }
    if(validator.isEmpty(_.get(req, 'body.no_of_player'))) {
      customValidationMsg('No of player is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.playerId'))) {
      customValidationMsg('Played Id is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.walletId'))) {
      customValidationMsg('Wallet Id is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.challenge_fee', ''))) {
      customValidationMsg('Challenge fee is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.selected_slot', ''))) {
      customValidationMsg('Selected Slot is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.win_amount', ''))) {
      customValidationMsg('Win Amount is required.', res);
    }else if(_.get(req, 'body.slots', []).length < 1) {
      customValidationMsg('Slots is required.', res);
    }
    else{
       saveChallenge()
       .then((response)=>{
         return getChallengeInfo(response)
       })
      .then(function(response){
        emitSocket(response);
        var data = {detail: response, token: _.get(req, 'body.token', ''), status : true, message : 'Challenge created successfully.'};
        var json_arr  = {data: data, success: true};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
      })
      .catch(function(err){
          var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
      })
    }
  })


  /** Api to accept challenge  **/  
  app.post('/acceptChallenge', function(req, res){

    function getChallengeInfo(resp){

      return new Promise((resolve, reject)=>{
        challenge.getChallengeById(_.get(req, 'body.challengeId'), (err, data)=>{
          if(err)
          {
            reject(err)
          }else
          {
            if(_.isEmpty(resp))
            {
              resolve(data)
            }
            else
            {
              resolve([resp, data]);
            }
          }
        })
      })
    }

    function acceptChallenge( resp){
      return new Promise((resolve, reject)=>{
        var datatoupdate = {
          playerId : _.get(req, 'body.playerId'),
          walletId : _.get(req, 'body.walletId'),
          slot : _.get(req, 'body.selected_slot', '')
        };
        var status = ((_.get(resp, 'remainingPlayer')  <=1) ? 'start' : 'open');


        challenge.updateChallenge(_.get(req, 'body.challengeId'),  _.get(req, 'body.selected_slot', ''), status, datatoupdate, (err, response) =>{
          if(err){
            reject(err)
          }
          else
          {
            resolve({"data" : "success"});
          }
        })
      })
    }

    function processResult(collectedSlot, challengeId){
      return new Promise((resolve, reject)=>{
        function between(x, min, max) {
          return x >= min && x <= max;
        }
        var luckyNumber = Math.floor(Math.random() * ((process.env.maxNum || 100) - (process.env.minNum || 0)));
        for (var i=0; i< collectedSlot.length; i++){
          var slot = _.get(collectedSlot[i], 'slot', "").split('-');
          var min = parseInt(slot[0]);
          var max = parseInt(slot[1]);
          var winner;
          if(between(luckyNumber, min, max )){
            winner = _.get(collectedSlot[i], 'playerId', null);
          }
        }
         challenge.updateWinner(challengeId, winner, luckyNumber, 'close', (err, response)=>{
            if(err){
              reject(err);
            }
            else{
              resolve(response);
            }
          })
      })
    }
    function transferFundsToWinner(result){
      
      console.log('===============>', result.winner.walletid)
       console.log('Wallet Id : ', _.get(result, 'winner.walletid', '')); 
    //vidit is awarded 1500 TRX for challenge challengename


      // account for croupier
      var tronWeb = new TronWeb({
          fullNode: 'https://api.shasta.trongrid.io',
          solidityNode: 'https://api.shasta.trongrid.io',
          eventServer: 'https://api.shasta.trongrid.io',
          privateKey: process.env.croupier_private_key
      })
      console.log("transfer funds");
      console.log('Wallet Id : ', _.get(result, 'winner.walletid', ''));
      return new Promise((resolve,reject) => {
        let to = _.get(result, 'winner.walletid', '');//winner wallet id
        let amount =  (_.get(result, 'winAmount', 0) * 1000000);//win amount
        console.log('Win Amount : ', _.get(result, 'winAmount', 0));
        console.log('Amount : ', amount);

        tronWeb.trx.sendTransaction(to, amount).then((res) => {
          console.log("transfer Funds res" , res);
          resolve(result)
        }, err => {
          console.log("transfer funds error", err);
          reject(err);
        })
      })
    }
    function calculateResult(response)
    {
      if(response){
           var collectedSlot = _.get(response[1], 'selectedSlot', []);
           var challengeId = _.get(response[1], '_id', []);
           if(collectedSlot.length){
              processResult(collectedSlot,challengeId)
              .then((result)=>{
                global.io.emit("challenge_finish", {details : result, message : "challenge finished"} );
                 return transferFundsToWinner(result);
              })
              .then((result) =>{
                  global.io.emit("payment_done", {details : result, message : _.get(result,'winner.fname', '') +" is awarded "+_.get(result, 'winAmount', 0)+" TRX for challenge "+ _.get(result,'title', '') } );
              })
              .catch((err) => {
                console.log('Error in Challenge Finished', err );
                global.io.emit("challenge_finish", {details : {err: err }, message : "challenge finished contains error"} );
              })
           }else{
              console.log('Slot cannot be blank', collectedSlot );
           }
      }
    }



    function emitAccepted(response){
        global.io.emit('challenge_accepted' ,{details : response[1], message : _.get(response[1], 'title', "") + " challenge is accepted by "+  (_.get(req, 'body.playerName') ?  _.get(req, 'body.playerName') : _.get(req, 'body.walletId'))
      } );
    }

    function emitStarted(response){
      global.io.emit("challenge_started", {details : response[1], message : "challenge started"} );
    }
    if(validator.isEmpty(_.get(req, 'body.challengeId'))) {
      customValidationMsg('Challenge Id is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.playerId'))) {
      customValidationMsg('Played Id is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.walletId'))) {
      customValidationMsg('Wallet Id is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.selected_slot', ''))) {
      customValidationMsg('Selected Slot is required.', res);
    }else{
      getChallengeInfo('')
      .then((response)=>{
          if(Object.keys(response).length)
          {
            if((_.get(response, 'slotOccupied').length > 0) && (_.get(response, 'slotOccupied').indexOf(_.get(req, 'body.selected_slot')) > -1)){
              return ({"data" : "taken"});
            }
            else if(_.get(response, 'remainingPlayer')>=1){
               return acceptChallenge(response)
            }
            else{
              return ({"data" : "done"})
            }
          }
          else
          {
            return ({"data" : "not exist"})
          }
      })
      .then((response)=>{
        
        return getChallengeInfo(response)
      })
      .then((response)=>{
          if(_.get(response[0], 'data') == 'taken'){
            var data = {detail: response[1], token: _.get(req, 'body.token', ''), status : true, message : 'Sorry! This slot is allready taken by someone.'};
            var json_arr  = {data: data, success: false};
          }
          else{
            if(_.get(response[0], 'data') != 'done' && _.get(response[1], 'status') == "start"){
              emitAccepted(response);
              emitStarted(response);
              calculateResult(response);
            }
            if(_.get(response[0], 'data') == 'success')
            {
              emitAccepted(response);
              var data = {detail: response[1], token: _.get(req, 'body.token', ''), status : true, message : 'Challenge accepted successfully.'};
              var json_arr  = {data: data, success: true};
            }
            else if(_.get(response[0], 'data') == 'done'){
              var data = {detail: {}, token: _.get(req, 'body.token', ''), status : true, message : 'No more participants.'};
              var json_arr  = {data: data, success: false};
            }
            else
            {
              var data = {detail: {}, token: _.get(req, 'body.token', ''), status : true, message : 'Challenge not exist.'};
              var json_arr  = {data: data, success: false};
            }
          }
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));      
      })
      .catch((err)=>{
          var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
      })
    }
  })

 /** Api to get challenge created by player id  **/  
  app.post('/getChallenge', function(req, res){

    function getChallenge(){
      return new Promise((resolve, reject)=>{
        challenge.getChallengeBycreator(_.get(req, 'body.playerId'), (err, data)=>{
          if(err){
            reject(err)
          }else{
            resolve(data);
          }
        })
      })
    }
    if(validator.isEmpty(_.get(req, 'body.playerId'))) {
      customValidationMsg('player Id is required.', res);
    }
    else{
      getChallenge()
      .then((response)=>{
        var data = {detail: response, token: _.get(req, 'body.token', ''), status : true, message : 'Detail found successfully'};
        var json_arr  = {data: data, success: true};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
      })
      .catch((err)=>{
          var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
      })
    }
  })

  /** Api to get accepted challenge by player id  **/  
  app.post('/getAcceptedChallenge', function(req, res){

    function getChallenge(){
      return new Promise((resolve, reject)=>{
        challenge.getChallengeByPlayerId(_.get(req, 'body.playerId'), (err, data)=>{
          if(err){
            reject(err)
          }else{
            resolve(data);
          }
        })
      })
    }
    if(validator.isEmpty(_.get(req, 'body.playerId'))) {
      customValidationMsg('player Id is required.', res);
    }
    else{
      getChallenge()
      .then((response)=>{
        var data = {detail: response, token: _.get(req, 'body.token', ''), status : true, message : 'Detail found successfully'};
        var json_arr  = {data: data, success: true};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
      })
      .catch((err)=>{
          var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
      })
    }
  })

app.post('/getOpenChallenge', function(req, res){

    function getOpenChallenge(){
      return new Promise((resolve, reject)=>{
        challenge.getChallengeByStatus('open', (err, data)=>{
          if(err){
            reject(err)
          }else{
            resolve(data);
          }
        })
      })
    }
    getOpenChallenge()
      .then((response)=>{
        var data = {detail: response, token: _.get(req, 'body.token', ''), status : true, message : 'Detail found successfully'};
        var json_arr  = {data: data, success: true};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
      })
      .catch((err)=>{
          var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
      })
  })

/** Api to get challenge bu player id  **/  
  app.post('/getChallengeById', function(req, res){

    function getChallenge(){
      return new Promise((resolve, reject)=>{
        challenge.getChallengeById(_.get(req, 'body.challengeId'), (err, data)=>{
          if(err){
            reject(err)
          }else{
            resolve(data);
          }
        })
      })
    }
    if(validator.isEmpty(_.get(req, 'body.challengeId'))) {
      customValidationMsg('Challenge Id is required.', res);
    }
    else{
      getChallenge()
      .then((response)=>{
        var data = {detail: response, token: _.get(req, 'body.token', ''), status : true, message : 'Detail found successfully'};
        var json_arr  = {data: data, success: true};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
      })
      .catch((err)=>{
          var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
      })
    }
  })

  /** Api to accept challenge  **/  
  app.post('/recentChallenge', function(req, res){
    function getRecentChallenges(){
      return new Promise(function(resolve, reject){
        let tmpCond = {};

        if(_.get(req, 'body.userid') && req.body.userid !=''){
          tmpCond = {'status' :'close', "$and":[{"selectedSlot.playerId": req.body.userid}]}
        }
        else
        {
          tmpCond = {'status' : 'close'};
        }
        let page = (_.get(req, 'body.page') && req.body.page !='') ? req.body.page : 1;
        let perPage = (_.get(req, 'body.perPage') && req.body.perPage !='') ? req.body.perPage : 1000;
        challenge.getChallengeData(page, perPage, tmpCond, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    getRecentChallenges().then(function(response) {
        var data = {challenges: response.challenge, status : true, message : 'Listing of Recent Challenge.'};
        var json_arr  = {data: data, success: true};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
    })
    .catch(function(derr,err){
        var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
        var json_arr  = {data: data, success: false};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
    }) 
  })
}