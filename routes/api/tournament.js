exports = module.exports = function(app, user, bet, point, challenge, tournament, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, _){
   
  var prizeMatrix = require('../../utils/prizeMatrix');

  app.get('/api/checkDate', function(req, res){

    function publishTournament(){
      return new Promise(function(resolve, reject){

        // let startDate = new Date();
        // startDate.setHours(0,0,0,0);
        let startDate = new Date().toLocaleString('en-US', {'timeZone': 'Asia/Calcutta'});


        let endDate = new Date(); 
        // endDate.setHours(23,59,59,999);
        let myquery = {status: 'Upcoming', startdate:  {"$lt": new Date(startDate)}};

        
        // console.log('startDate, utcDate, mDate, m_after_utc', startDate, utcDate, mDate, m_after_utc);
        // let myquery = {status: 'Upcoming', startdate: startDate.toUTCString()}; 
        // console.log('myquery', myquery);   
        let jsondata = {status:'Ongoing'}
        tournament.getTournamentByStatus(myquery, {}, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    } 
    publishTournament()
    .then(function(response){
      let startDate = new Date();
      let mDate = MOMENT(startDate).format("YYYY-MM-DD HH:mm:ss");
        let utcDate = startDate.toUTCString();
        let m_after_utc = MOMENT(utcDate).format("YYYY-MM-DD HH:mm:ss");
      var json_arr  = {data: response, 'startDate': startDate, 'utcDate': utcDate, 'mDate': mDate, 'm_after_utc': m_after_utc, success: true, message : 'tournaments fetched successfully'};
      res.contentType('application/json');
      res.end(JSON.stringify(json_arr));
    })
    .catch(function(derr, err){
      var json_arr  = {status : 0, error: err , pages:'', success: false, message : 'Oops!! An error occured while updating details.Please try again'};
      res.contentType('application/json');
      res.end(JSON.stringify(json_arr));
    })
  });

  app.post('/api/getTournaments', function(req, res){
    function getTournamentData(){
      let condData = {"name": { "$nin": [ null, "" ]}};
      let fields = {'name': true,'status': true,'manual': true};
      return new Promise(function(resolve, reject){
        tournament.getAllTournaments(condData, fields, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            resolve(response);
          }
        })
      })
    }
    getTournamentData()
    .then(function(response){
      var json_arr  = {data: ((response.tournaments) ? response.tournaments : []), success: true, message : 'tournaments fetched successfully'};
      res.contentType('application/json');
      res.end(JSON.stringify(json_arr));
    })
    .catch(function(derr, err){
      var resdata = {status : 0, error: err , pages:''};
      var json_arr  = {tournaments: resdata, success: false, message : 'Oops!! An error occured while updating details.Please try again'};
      res.contentType('application/json');
      res.end(JSON.stringify(json_arr));
    }) 
  })

  /** Api to get live tournament response **/  
  app.post('/getLiveTournament', function(req, res){
    function getLive(){
      return new Promise(function(resolve, reject){ 
      let condData = {"name": { "$nin": [ null, "" ]}};   
        tournament.getActiveTournaments(condData, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function getBetDetailByTournamentId(tournamentid, playerid){
      return new Promise(function(resolve, reject){    
        bet.getBetsCountByPlayer(tournamentid, playerid, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    getLive()
    .then(function(response){
        var totRes = response.length;
        _.forEach(response, function(value, key) {
          let isEnrolled  = false;
          let isPlayed  = false;
          //calculate winning amt
          if(value.entry_fee > 0){
            let winning_amt = (value.players.length * value.entry_fee)*.95;
            response[key].winning_amount = winning_amt;
          }
          if(value.entry_fee==0){
            let winning_amt = response[key].winning_amount*.95;
            response[key].winning_amount = winning_amt;
          }
          if(_.get(req, 'body.userid') && req.body.userid !='' && value.players){
            let tmpuserid = req.body.userid;
            isEnrolled = _.find(value.players, (player) => (player && player.playerId._id == tmpuserid));
            
            isEnrolled = (isEnrolled && isEnrolled != undefined && isEnrolled !='') ? true : false;
            
            response[key].isEnrolled = isEnrolled;
            
          
            let noOfPlayer = (value.noOfPlayer) ? value.noOfPlayer : 10;
            let playersLeft = value.noOfPlayer - value.players.length;
            response[key].playersLeft = (playersLeft) ? playersLeft : 0;
            response[key].winning_prizes = prizeMatrix.calculatePrize(noOfPlayer);
            getBetDetailByTournamentId(value._id, tmpuserid)
              .then(function(bres){
                isPlayed = (bres && bres> 0) ? true : false;
                response[key].isPlayed = isPlayed;
                if(key == totRes - 1) {
                  var data = {detail: response, status : true, message : 'Listing of Acitve tournament.'};
                  var json_arr  = {data: data, success: true};
                  res.contentType('application/json');
                  res.end(JSON.stringify(json_arr));
                }
              })
          }
        })
        if(!_.get(req, 'body.userid') || req.body.userid ==''){
          var data = {detail: response, status : true, message : 'Listing of Acitve tournament.'};
          var json_arr  = {data: data, success: true};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
        }
        
    })
    .catch(function(derr,err){
        var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
        var json_arr  = {data: data, success: false};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
    })
  })

  /** Api to save tournament winners **/
  app.post('/saveTournamentWinner', function(req, res){
    function checkUser(){
      return new Promise(function(resolve, reject){    
        user.getUserById(req.body.playerid, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function checkTournament(){
      return new Promise(function(resolve, reject){    
        tournament.getTournamentById(req.body.tournamentid, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function saveWinner() {
      return new Promise(function(resolve, reject){
        tournament.updateWinner(req.body.tournamentid, req.body.playerid, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
              resolve(response);
            }
        })
      })
    }
    if(validator.isEmpty(_.get(req, 'body.walletid'))) {
      customValidationMsg('Wallet Id is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.playerid', ''))) {
      customValidationMsg('Player Id is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.tournamentid', ''))) {
      customValidationMsg('Tournament Id is required.', res);
    }
    else{
      var checkUser =  checkUser();
      var checkTournament =  checkTournament();
      var saveWinner = saveWinner();
      Promise.all([checkUser, checkTournament, saveWinner]).then(function(response) {
        var data = {detail: response[2], status : true, };
        res.contentType('application/json');
        res.end(JSON.stringify(data));
      })
      .catch(function(derr, err){
        let errtxt = (err) ? err : '';
        var data = {status : 0, error: errtxt , message : 'Oops!! An error occured while processing request.Please try again'};
        var json_arr  = {data: data, success: false};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
      })
    }
  })

  /** Api to play tournament **/
  app.post('/playTournament', function(req, res){
    function checkUser(){
      return new Promise(function(resolve, reject){    
        user.getUserById(req.body.playerid, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function checkTournament(){
      return new Promise(function(resolve, reject){    
        tournament.getTournamentById(req.body.tournamentid, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function playTournament() {
      return new Promise(function(resolve, reject){
        let userID = req.body.playerid;
        let betId = req.body.betid;
        let rawData = {
          $push: {players: {playerId: userID}}
        }
        tournament.playTournament(req.body.tournamentid, userID, betId, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
              resolve(response);
            }
        })
      })
    }
    if(validator.isEmpty(_.get(req, 'body.walletid'))) {
      customValidationMsg('Wallet Id is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.playerid', ''))) {
      customValidationMsg('Player Id is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.tournamentid', ''))) {
      customValidationMsg('Tournament Id is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.betid', ''))) {
      customValidationMsg('Bet Id is required.', res);
    }
    else{
      var checkUser =  checkUser();
      var checkTournament =  checkTournament();
      Promise.all([checkUser, checkTournament]).then(function(response) {
        let userID = req.body.playerid;
        var userExists = _.filter(response[1].players, x => x.playerId._id == userID);
        if(userExists.length == 0){
          var data = {status : 0, error: '' , message : 'Player not enrolled in this tournament!'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
        }
        else
        {
          playTournament()
          .then(function(response) {
            var data = { status : true, message : 'User played with tournament'};
            var json_arr  = {data: data, success: true};
            res.contentType('application/json');
            res.end(JSON.stringify(json_arr));
          })
          .catch(function(derr, err){
            let errtxt = (err) ? err : '';
            var data = {status : 0, error: errtxt , message : 'Oops!! An error occured while processing request.Please try again'};
            var json_arr  = {data: data, success: false};
            res.contentType('application/json');
            res.end(JSON.stringify(json_arr));
          })
        }
      })
      .catch(function(derr, err){
        let errtxt = (err) ? err : '';
        var data = {status : 0, error: errtxt , message : 'Oops!! An error occured while processing request.Please try again'};
        var json_arr  = {data: data, success: false};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
      })
    }
  })

  /** Api to enroll players in  tournament **/
  app.post('/enrollPlayer', function(req, res){
    function checkUser(){
      return new Promise(function(resolve, reject){    
        user.getUserById(req.body.playerid, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function checkTournament(){
      return new Promise(function(resolve, reject){    
        tournament.getTournamentById(req.body.tournamentid, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function enrollPlayer() {
      return new Promise(function(resolve, reject){
        let userID = req.body.playerid;
        let rawData = {
          $push: {players: {playerId: userID, enroll_date: new Date().toDateString(), playerBet:[]}}
        }
        tournament.enrollPlayer(req.body.tournamentid, rawData, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
              resolve(response);
            }
        })
      })
    }
    if(validator.isEmpty(_.get(req, 'body.walletid'))) {
      customValidationMsg('Wallet Id is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.playerid', ''))) {
      customValidationMsg('Player Id is required.', res);
    }else if(validator.isEmpty(_.get(req, 'body.tournamentid', ''))) {
      customValidationMsg('Tournament Id is required.', res);
    }
    else{
      var checkUser =  checkUser();
      var checkTournament =  checkTournament();
      Promise.all([checkUser, checkTournament]).then(function(response) {
        let userID = req.body.playerid;
        var userExists = _.filter(response[1].players, x => x.playerId._id == userID);
        if(userExists.length > 0){
          var data = {status : 0, error: '' , message : 'Player already enrolled in this tournament!'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
        }
        else
        {
          enrollPlayer()
          .then(function(response) {
            var data = { status : true, message : 'Player enrolled successfully.'};
            var json_arr  = {data: data, success: true};
            res.contentType('application/json');
            res.end(JSON.stringify(json_arr));
          })
          .catch(function(derr, err){
            let errtxt = (err) ? err : '';
            var data = {status : 0, error: errtxt , message : 'Oops!! An error occured while processing request.Please try again'};
            var json_arr  = {data: data, success: false};
            res.contentType('application/json');
            res.end(JSON.stringify(json_arr));
          })
        }
      })
      .catch(function(derr, err){
        let errtxt = (err) ? err : '';
        var data = {status : 0, error: errtxt , message : 'Oops!! An error occured while processing request.Please try again'};
        var json_arr  = {data: data, success: false};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
      })
    }
  })

  /** Api to get tournament  **/  
  app.post('/getTournament', function(req, res){

    function getTournamentById(){
      return new Promise(function(resolve, reject){    
        tournament.getTournamentById(_.get(req, 'body.tournamentid'), function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function getTournamentInfoById(){
      return new Promise(function(resolve, reject){    
        tournament.getTournamentInfoById(_.get(req, 'body.tournamentid'), function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function getBetDetailByTournamentId(){
      return new Promise(function(resolve, reject){    
        bet.getBetsByTournamentId(_.get(req, 'body.tournamentid'), function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function getPointByTournamentId(){
      return new Promise(function(resolve, reject){    
        point.getPointByTournamentId(_.get(req, 'body.tournamentid'), function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function getLeaderBoard(){
      return new Promise(function(resolve, reject){
        point.getLeaderBoard(req.body.tournamentid, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    if(validator.isEmpty(_.get(req, 'body.tournamentid'))) {
      customValidationMsg('Tournament Id is required.', res);
    }
    else{
      var getTournament =  getTournamentById();
      var getBetDetail =  getBetDetailByTournamentId();
      var getTournamentInfoById =  getTournamentInfoById();
      var getPointByTournamentId = getPointByTournamentId();
      var getLeaderBoard = getLeaderBoard();
      Promise.all([getTournament, getBetDetail, getTournamentInfoById, getPointByTournamentId, getLeaderBoard]).then(function(response) {
        let isPlayed  = false;
        let isEnrolled = false;
        let tmpResponse = (response[2].length > 0) ? response[2][0] : response[0];
        
        //calculate winning amt
        if(tmpResponse.entry_fee > 0){
          let winning_amt = (tmpResponse.players.length * tmpResponse.entry_fee)*.95;
          tmpResponse.winning_amount = winning_amt;
        }
        if(tmpResponse.entry_fee==0){
          let winning_amt = tmpResponse.winning_amount*.95;
          tmpResponse.winning_amount = winning_amt;
        }
        if(_.get(req, 'body.userid') && req.body.userid !='' && tmpResponse.players){
          let tmpuserid = req.body.userid;
          isEnrolled = _.find(tmpResponse.players, (player) => (player && player.playerId._id == tmpuserid));
          
          tmpResponse.userInfo = false;
          if(isEnrolled && isEnrolled.playerId){
            tmpResponse.userInfo = isEnrolled.playerId;    
          }
          // isPlayed = _.find(tmpResponse.players, (player) => (player && player.playerId._id == tmpuserid && player.playerBet && player.playerBet.length > 0)); 
          isEnrolled = (isEnrolled && isEnrolled != undefined && isEnrolled !='') ? true : false;
          // isPlayed = (isPlayed && isPlayed != undefined && isPlayed !='') ? true : false;
          
          isPlayed = _.find(response[1], (player) => (player && player.playerId == tmpuserid)); 
          isPlayed = (isPlayed && isPlayed != undefined && isPlayed !='') ? true : false;
        }
        
        let prizeAmt = tmpResponse.winning_amount;
        let playerArr = response[4];
        let noOfPlayer = (tmpResponse.noOfPlayer) ? tmpResponse.noOfPlayer : 10;
        let totalWinPlayer = prizeMatrix.calculatePrize(noOfPlayer);
       
        if(tmpResponse.players.length > 0 && playerArr.length> 0){
          _.forEach(playerArr, function(value, key) {
            let playerWinAmt = prizeAmt * (totalWinPlayer[key]/100);
            playerArr[key].expected_gain = Math.round(playerWinAmt);
            playerArr[key].win_percentage = totalWinPlayer[key];
            playerArr[key].user_position = (key + 1);
          })
        }


        tmpResponse.isEnrolled = isEnrolled;
        tmpResponse.isPlayed = isPlayed;
        
        tmpResponse.winning_prizes = prizeMatrix.calculatePrize(noOfPlayer);
        let playersLeft = tmpResponse.noOfPlayer - tmpResponse.players.length;
        tmpResponse.playersLeft = (playersLeft) ? playersLeft : 0;
        var data = {detail: tmpResponse , bets:response[1], points:response[3], leaderboard:playerArr, isPlayed: isPlayed, status : true, message : 'Tournament Detail found successfully'};
        var json_arr  = {data: data, success: true};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
      })
      .catch((err)=>{
          var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
      })
    }
  })

  /** Api to get tournament  by player id**/  
  app.post('/getTournamentByUserId', function(req, res){

    function getTournamentByUserId(){
      return new Promise(function(resolve, reject){    
        tournament.getTournamentByPlayerId(_.get(req, 'body.userid'), function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    if(validator.isEmpty(_.get(req, 'body.userid'))) {
      customValidationMsg('Player Id is required.', res);
    }
    else{
      getTournamentByUserId()
      .then((response)=>{
        var data = {detail: response, status : true, message : 'Tournament Details found successfully'};
        var json_arr  = {data: data, success: true};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
      })
      .catch((err)=>{
          var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
      })
    }
  })

  /** Api to get tournament  by game id**/  
  app.post('/getTournamentByGameId', function(req, res){

    function getTournamentByGameId(){
      return new Promise(function(resolve, reject){    
        tournament.getTournamentByGameId(_.get(req, 'body.gameid'), function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    if(validator.isEmpty(_.get(req, 'body.gameid'))) {
      customValidationMsg('Game Id is required.', res);
    }
    else{
      getTournamentByGameId()
      .then((response)=>{
        var data = {detail: response, status : true, message : 'Tournament Details found successfully'};
        var json_arr  = {data: data, success: true};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
      })
      .catch((err)=>{
          var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
      })
    }
  })

  /** Api to get won tournament  by player id**/  
  app.post('/getWonTournamentByUserId', function(req, res){

    function getWonTournamentByUserId(){
      return new Promise(function(resolve, reject){    
        tournament.getWonTournamentByUserId(_.get(req, 'body.userid'), function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    if(validator.isEmpty(_.get(req, 'body.userid'))) {
      customValidationMsg('Player Id is required.', res);
    }
    else{
      getWonTournamentByUserId()
      .then((response)=>{
        var data = {detail: response, status : true, message : 'Tournament Details found successfully'};
        var json_arr  = {data: data, success: true};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
      })
      .catch((err)=>{
          var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
      })
    }
  })

  /** Api to get Upcomming tournament response **/  
  app.post('/getUpcommingTournament', function(req, res){
    function getUpcomming(){
      return new Promise(function(resolve, reject){    
        tournament.getTournamentByStatus({"name": { "$nin": [ null, "" ]}, 'status' : 'Upcoming'},{}, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    getUpcomming()
    .then(function(response){
        _.forEach(response.tournaments, function(value, key) {
          let isEnrolled  = false;
          //calculate winning amt
          if(value.entry_fee > 0){
            let winning_amt = (value.players.length * value.entry_fee)*.95;
            response.tournaments[key].winning_amount = winning_amt;
          }
          if(value.entry_fee==0){
            let winning_amt = value.winning_amount*.95;
            response.tournaments[key].winning_amount = winning_amt;
          }
          if(_.get(req, 'body.userid') && req.body.userid !='' && value.players){
            let tmpuserid = req.body.userid;
            isEnrolled = _.find(value.players, (player) => (player && player.playerId._id == tmpuserid));
            isEnrolled = (isEnrolled && isEnrolled != undefined && isEnrolled !='') ? true : false;
            response.tournaments[key].isEnrolled = isEnrolled;
          }
          let noOfPlayer = (value.noOfPlayer) ? value.noOfPlayer : 10;
          response.tournaments[key].winning_prizes = prizeMatrix.calculatePrize(noOfPlayer);
          let playersLeft = value.noOfPlayer - value.players.length;
          response.tournaments[key].playersLeft = (playersLeft) ? playersLeft : 0;
        })
        var data = {detail: response, status : true, message : 'Listing of Upcoming tournament.'};
        var json_arr  = {data: data, success: true};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
    })
    .catch(function(derr,err){
        var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
        var json_arr  = {data: data, success: false};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
    })
  })

  /** Api to get Finished tournament response **/  
  app.post('/getFinishedTournament', function(req, res){
    function getPrevious(){
      return new Promise(function(resolve, reject){
        let tmpCond = {};

        if(_.get(req, 'body.userid') && req.body.userid !=''){
          tmpCond.userid = req.body.userid;
          tmpCond = {"name": { "$nin": [ null, "" ]}, 'status' : 'Finished'};
          // tmpCond = {"name": { "$nin": [ null, "" ]}, 'status' : 'Finished','players.playerId' : mongoose.Types.ObjectId(_.get(req, 'body.userid'))};
        }
        else
        {
          tmpCond = {"name": { "$nin": [ null, "" ]}, 'status' : 'Finished'};
        }
        let page = (_.get(req, 'body.page') && req.body.page !='') ? req.body.page :1;
        let perPage = (_.get(req, 'body.perPage') && req.body.perPage !='') ? req.body.perPage : null; 
        tournament.getTournamentByStatus(tmpCond, {page : page, perPage: perPage}, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function getBetDetailByTournamentId(tournamentid, playerid){
      return new Promise(function(resolve, reject){    
        bet.getBetsCountByPlayer(tournamentid, playerid, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    getPrevious()
    .then(function(response){
        let totRes = response.tournaments.length;
         _.forEach(response.tournaments, function(value, key) {
          let isEnrolled  = false;
          let isPlayed  = false;
          //calculate winning amt
          if(value.entry_fee > 0){
            let winning_amt = (value.players.length * value.entry_fee)*.95;
            response.tournaments[key].winning_amount = winning_amt;
          }
          if(value.entry_fee==0){
            let winning_amt = response.tournaments[key].winning_amount*.95;
            response.tournaments[key].winning_amount = winning_amt;
          }

          let noOfPlayer = (value.noOfPlayer) ? value.noOfPlayer : 10;
          response.tournaments[key].winning_prizes = prizeMatrix.calculatePrize(noOfPlayer);
          let playersLeft = value.noOfPlayer - value.players.length;
          response.tournaments[key].playersLeft = (playersLeft) ? playersLeft : 0;

          if(_.get(req, 'body.userid') && req.body.userid !='' && value.players){
            let tmpuserid = req.body.userid;
            isEnrolled = _.find(value.players, (player) => (player && player.playerId._id == tmpuserid));
            isEnrolled = (isEnrolled && isEnrolled != undefined && isEnrolled !='') ? true : false;
            response.tournaments[key].isEnrolled = isEnrolled;
            response.tournaments[key].isPlayed = isPlayed;

            getBetDetailByTournamentId(value._id, tmpuserid)
              .then(function(bres){
                isPlayed = (bres && bres> 0) ? true : false;
                response.tournaments[key].isPlayed = isPlayed;
                if(key == totRes - 1) {
                  var data = {detail: response, status : true, message : 'Listing of Finished tournament.'};
                  var json_arr  = {data: data, success: true};
                  res.contentType('application/json');
                  res.end(JSON.stringify(json_arr));
                }
              })
          }
          
        })
        if(!_.get(req, 'body.userid') || req.body.userid ==''){
          var data = {detail: response, status : true, message : 'Listing of Finished tournament.'};
          var json_arr  = {data: data, success: true};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
        }
    })
    .catch(function(derr,err){
        var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
        var json_arr  = {data: data, success: false};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
    })
  })

  /** Api to get Finished tournament response **/  
  app.post('/getTournamentLeaderBoard', function(req, res){
    function getLeaderBoard(){
      return new Promise(function(resolve, reject){
        point.getLeaderBoard(req.body.tournamentid, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    if(validator.isEmpty(_.get(req, 'body.tournamentid'))) {
      customValidationMsg('Tournament Id is required.', res);
    }
    else{
      getLeaderBoard()
      .then(function(response){
          var data = {detail: response, status : true, message : 'Tournament Leaderboard.'};
          var json_arr  = {data: data, success: true};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
      })
      .catch(function(derr,err){
          var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
          var json_arr  = {data: data, success: false};
          res.contentType('application/json');
          res.end(JSON.stringify(json_arr));
      })
    }
  })

  
  /** Api to get 10 Recent game response **/  
  app.post('/getRecentGames', function(req, res){
    function getRecentTournaments(){
      return new Promise(function(resolve, reject){
        let tmpCond = {};

        if(_.get(req, 'body.userid') && req.body.userid !=''){
          tmpCond = {"name": { "$nin": [ null, "" ]}, 'status' :'Finished','players.playerId' : mongoose.Types.ObjectId(_.get(req, 'body.userid'))};
        }
        else
        {
          tmpCond = {"name": { "$nin": [ null, "" ]}, 'status' :'Finished'};
        }
        let page = (_.get(req, 'body.page') && req.body.page !='') ? req.body.page : 1;
        let perPage = (_.get(req, 'body.perPage') && req.body.perPage !='') ? req.body.perPage : 1000; 
        tournament.getTournamentByStatus(tmpCond, {page : page, perPage: perPage}, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function getRecentChallenges(){
      return new Promise(function(resolve, reject){
        let tmpCond = {};

        if(_.get(req, 'body.userid') && req.body.userid !=''){
          tmpCond = {'status' :'close', "$and":[{"selectedSlot.playerId": req.body.userid}]}
        }
        else
        {
          tmpCond = {'status' : 'close'};
        }
        let page = (_.get(req, 'body.page') && req.body.page !='') ? req.body.page : 1;
        let perPage = (_.get(req, 'body.perPage') && req.body.perPage !='') ? req.body.perPage : 1000;
        challenge.getChallengeData(page, perPage, tmpCond, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    var getRecentTournaments =  getRecentTournaments();
    var getRecentChallenges =  getRecentChallenges();
    Promise.all([getRecentTournaments, getRecentChallenges]).then(function(response) {
        let tmpTournaments = response[0].tournaments;
        _.forEach(tmpTournaments, function(value, key) {
          let isEnrolled  = false;
          let isPlayed  = false;
          //calculate winning amt
          if(value.entry_fee > 0){
            let winning_amt = (value.players.length * value.entry_fee)*.95;
            tmpTournaments[key].winning_amount = winning_amt;
          }

          if(value.entry_fee==0){
            let winning_amt = value.winning_amount*.95;
            tmpTournaments[key].winning_amount = winning_amt;
          }
          if(_.get(req, 'body.userid') && req.body.userid !='' && value.players){
            let tmpuserid = req.body.userid;
            isEnrolled = _.find(value.players, (player) => (player && player.playerId._id == tmpuserid));
            isPlayed = _.find(value.players, (player) => (player && player.playerId._id == tmpuserid && player.playerBet && player.playerBet.length > 0)); 
            isEnrolled = (isEnrolled && isEnrolled != undefined && isEnrolled !='') ? true : false;
            isPlayed = (isPlayed && isPlayed != undefined && isPlayed !='') ? true : false;
            tmpTournaments[key].isEnrolled = isEnrolled;
            tmpTournaments[key].isPlayed = isPlayed;
          }
          let noOfPlayer = (value.noOfPlayer) ? value.noOfPlayer : 10;
          tmpTournaments[key].winning_prizes = prizeMatrix.calculatePrize(noOfPlayer);
          let playersLeft = value.noOfPlayer - value.players.length;
          tmpTournaments[key].playersLeft = (playersLeft) ? playersLeft : 0;
        })
        var data = {tournaments: tmpTournaments, tournament_current_page : response[0].current, tournament_pages: response[0].pages, challenges: response[1].challenge, challenge_current_page : response[1].current, challenge_pages: response[1].pages, status : true, message : 'Listing of Recent Games.'};
        var json_arr  = {data: data, success: true};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
    })
    .catch(function(derr,err){
        var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
        var json_arr  = {data: data, success: false};
        res.contentType('application/json');
        res.end(JSON.stringify(json_arr));
    })
  })

} 