exports = module.exports = function(app, user, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, cryptr, _){
  
app.post('/login', function(req, res, next){
  function signin(){
    return new Promise(function(resolve, reject){
      user.getUserByUsername(_.get(req, 'body.username'), function(err, response){

          if(err)
          {
              reject('noRecord', new Error(err));
          }
          else
          {
          if(_.isEmpty(response))
          {
            reject('username', null)
          }
          else if(!response.password)
          {
            reject('invalid', null)
          }
          else if(passwordHash.verify(_.get(req, 'body.password'), _.get(response, 'password', '')) && _.get(response, 'user_type', '') =='admin')
          {
            resolve(response)
          }
          else{
            reject('invalid', null)
          }
          }
      })
    })
  }
  if(validator.isEmpty(_.get(req, 'body.username', '')))
  {
    var data = {status : false, error: 'username is required.', message : '', page:'Login', title: 'User Login'};
    res.contentType('application/json');
    res.end(JSON.stringify(data));
  }
  else if(validator.isEmpty(_.get(req, 'body.password')))
  {
    var data = {status : false, error: 'Password is required.', message : '', page:'Login', title: 'User Login'};
    res.contentType('application/json');
    res.end(JSON.stringify(data));
  }
  else{
    signin()
    .then(function(response){
        const payload = {
          userid: response._id   
        };
        var token = jwt.sign(payload, process.env.key, {
          expiresIn: process.env.tokenexp
        });
        var sess = req.session; 
        var data = {detail: response, token:token, status :true, message : 'You have Logged-in successfully.'};
        res.contentType('application/json');
        res.end(JSON.stringify(data));
    })
    .catch(function(derr,err){
      if(derr == 'username' || derr == 'noRecord')
      {
        var data = {detail: {}, status : false, error : 'Sorry! You are not registered with us.', message : '', page:'Login', title: 'User Login'};
      }
      else if(derr == 'invalid')
      {
        var data = {status : false, error: 'Invalid Credentials.' , details:'', page:'Login', message : '', title: 'User Login'};
      }
      else if(derr == 'required')
      {
        var data = {status : false, error: err , details:'', message : '', page:'Login', title: 'User Login'};
      }
      else{
        var data = {status : false, error: 'Oops!! An error occured while processing request.Please try again', message : '', page:'Login', title: 'User Login'};
      }
      res.contentType('application/json');
      res.end(JSON.stringify(data));
    })
  } 
})

app.post('/getusers', function(req, res){
  function getUserData(){
    var perPage = 10;
    var page = req.body.page || 1
    let condData = {user_type: {'$ne':'admin'}};
    return new Promise(function(resolve, reject){
      user.getUserData(page, perPage, condData, function(err, response){
        if(err)
        {
          reject(null, new Error(err));
        }
        else
        {
          resolve(response);
        }
      })
    })
  }
  getUserData()
  .then(function(response){
    var data = {users: response.users, current: response.current, pages: response.pages};
    var json_arr  = {data: data, success: true, message : 'users fetched successfully'};
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
  })
  .catch(function(derr, err){
    var resdata = {status : 0, error: err , pages:''};
    var json_arr  = {data: resdata, success: false, message : 'Oops!! An error occured while updating details.Please try again'};
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
  }) 
})

app.post('/api/getallusers', function(req, res){
  function getUserData(){
    let condData = {user_type: {'$ne':'admin'}};
    return new Promise(function(resolve, reject){
      user.getAllUserData(condData, function(err, response){
        if(err)
        {
          reject(null, new Error(err));
        }
        else
        {
          resolve(response);
        }
      })
    })
  }
  getUserData()
  .then(function(response){
    var json_arr  = {data: ((response.users) ? response.users : []), success: true, message : 'users fetched successfully'};
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
  })
  .catch(function(derr, err){
    var resdata = {status : 0, error: err , pages:''};
    var json_arr  = {users: resdata, success: false, message : 'Oops!! An error occured while updating details.Please try again'};
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
  }) 
})

/** Api to signup user **/  
app.post('/sso', function(req, res){
  function checkUser(){
    return new Promise(function(resolve, reject){
      user.getUserByWalletId(_.get(req, 'body.walletid'), function(err, response){
        if(err)
        {
          reject(null, new Error(err));
        }
        else
        {
          // if(_.get(response, 'username') && _.get(req, 'body.username') == response.username)
          // {
          //   reject('username', null)
          // }
          // else if(_.get(response, 'email') && _.get(req, 'body.email') == response.email)
          // {
          //   reject('email', null)
          // }
          if(_.get(response, 'is_verified')  == 'no' || _.get(response, 'status')  == 'inactive')
          {
            reject('inactive', null)
          }
          else if(!_.isEmpty(response))
          {
            resolve(response)
          }
          else{
            resolve(response)
          }
        }
      })
    })
  }
  function signup(){
    return new Promise(function(resolve, reject){
      var userData = {
        walletid: req.body.walletid,
        status: 'active',
        user_type: 'user',
        is_verified:'yes'
      } 
      if(!_.isEmpty(req.body.email)){
        userData.email =req.body.email
      }
      if(!_.isEmpty(req.body.username)){
        userData.username =req.body.username
      }
      if(!_.isEmpty(req.body.fname)){
        userData.fname =req.body.fname
      }
      if(!_.isEmpty(req.body.lname)){
        userData.lname =req.body.lname
      }
      if(!_.isEmpty(req.body.age)){
        userData.age =req.body.age
      }
      if(!_.isEmpty(req.body.gender)){
        userData.gender =req.body.gender
      }
      if(!_.isEmpty(req.body.dailyDividend)){
        
        userData.userDividends = [{
          dailyDividend: req.body.dailyDividend
        }];
      }
      if(!_.isEmpty(req.body.password)){
        userData.password = passwordHash.generate(req.body.password)
      }

      if(!_.isEmpty(req.body.total_win_amount)){
        userData.total_win_amount =req.body.total_win_amount
      }
      if(!_.isEmpty(req.body.ImageUrl)){
        userData.ImageUrl =req.body.ImageUrl
      }
      if(!_.isEmpty(req.body.universal_level)){
        userData.universal_level =req.body.universal_level
      }
      if(!_.isEmpty(req.body.individual_game_level)){
        userData.individual_game_level =req.body.individual_game_level
      }
      userData.match_tokens = {};
      if(!_.isEmpty(req.body.canWithdraw)){
        userData.match_tokens.canWithdraw =req.body.canWithdraw
      }
      if(!_.isEmpty(req.body.canFreeze)){
        userData.match_tokens.canFreeze =req.body.canFreeze
      }
      if(!_.isEmpty(req.body.totalFrozen)){
        userData.match_tokens.totalFrozen =req.body.totalFrozen
      }
      user.createUser(userData, function(err, response){
        if(err)
        {
          reject(null, new Error(err));
        }
        else
        {
          resolve(response)
        }
      })
    })
  }
  function updateUser(uID){
    return new Promise(function(resolve, reject){
      let walletid = req.body.walletid;
      var userData = {
        user_type: 'user',
        is_verified:'yes'
      } 
      if(!_.isEmpty(req.body.email)){
        userData.email =req.body.email
      }
      if(!_.isEmpty(req.body.username)){
        userData.username =req.body.username
      }
      if(!_.isEmpty(req.body.fname)){
        userData.fname =req.body.fname
      }
      if(!_.isEmpty(req.body.lname)){
        userData.lname =req.body.lname
      }
      if(!_.isEmpty(req.body.age)){
        userData.age =req.body.age
      }
      if(!_.isEmpty(req.body.gender)){
        userData.gender =req.body.gender
      }
      if(!_.isEmpty(req.body.dailyDividend)){
        
        userData.userDividends = [{
          dailyDividend: req.body.dailyDividend
        }];
      }
      if(!_.isEmpty(req.body.password)){
        userData.password = passwordHash.generate(req.body.password)
      }

      if(!_.isEmpty(req.body.total_win_amount)){
        userData.total_win_amount =req.body.total_win_amount
      }
      if(!_.isEmpty(req.body.ImageUrl)){
        userData.ImageUrl =req.body.ImageUrl
      }
      if(!_.isEmpty(req.body.universal_level)){
        userData.universal_level =req.body.universal_level
      }
      if(!_.isEmpty(req.body.individual_game_level)){
        userData.individual_game_level =req.body.individual_game_level
      }
      userData.match_tokens = {};
      if(!_.isEmpty(req.body.canWithdraw)){
        userData.match_tokens.canWithdraw =req.body.canWithdraw
      }
      if(!_.isEmpty(req.body.canFreeze)){
        userData.match_tokens.canFreeze =req.body.canFreeze
      }
      if(!_.isEmpty(req.body.totalFrozen)){
        userData.match_tokens.totalFrozen =req.body.totalFrozen
      }
      user.update_commonById(uID, userData, function(err, response){
        if(err)
        {
          reject(null, new Error(err));
        }
        else
        {
          resolve(response)
        }
      })
    })
  }
  if(validator.isEmpty(_.get(req, 'body.walletid', '')))
  {
    customValidationMsg('Walletid is required.', res);
  }
  else{
    checkUser()
    .then(function(response){
        if(_.isEmpty(response)){
          signup()
          .then(function(resp){
              const payload = {
                userid: resp._id   
              };
              var token = jwt.sign(payload, process.env.key, {
                expiresIn: process.env.tokenexp
              });
              const encryptedtoken = cryptr.encrypt(token);
              var data = {detail: resp, status : true, token: encryptedtoken, message : 'You have successfully registered with us.'};
              var json_arr  = {data: data, success: true};
              res.contentType('application/json');
              res.end(JSON.stringify(json_arr));
          })
          .catch(function(derr,err){
            var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
            var json_arr  = {data: data, success: false};
            res.contentType('application/json');
            res.end(JSON.stringify(json_arr));
          })  
        }
        else
        {
          updateUser(response._id)
          .then(function(resp){
              const payload = {
                userid: resp._id   
              };
              var token = jwt.sign(payload, process.env.key, {
                expiresIn: process.env.tokenexp
              });
              const encryptedtoken = cryptr.encrypt(token);
              var data = {detail: resp, status : true, token: encryptedtoken, message : 'You have successfully Logged-in.'};
              var json_arr  = {data: data, success: true};
              res.contentType('application/json');
              res.end(JSON.stringify(json_arr));
          })
          .catch(function(derr,err){
            var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
            var json_arr  = {data: data, success: false};
            res.contentType('application/json');
            res.end(JSON.stringify(json_arr));
          })
        }
    })
    .catch(function(derr,err){
      if(derr == 'username')
      {
        var data = {detail: {}, status : 0, message : 'Sorry! Username already exists.'};
        var json_arr  = {data: data, success: false};
      }
      else if(derr == 'email')
      {
        var data = {detail: {}, status : 0, message : 'Email already exists.'};
        var json_arr  = {data: data, success: false};
      }
      else if(derr == 'invalid' || derr == 'inactive')
      {
        var data = {status : 0, error: err , details:'', message : 'Invalid details.'};
        var json_arr  = {data: data, success: false};
      }
      else{
        var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
        var json_arr  = {data: data, success: false};
      }
      res.contentType('application/json');
      res.end(JSON.stringify(json_arr));
    })
  }
})

//==========================================================================================================================================//

/** Api to get user profile  **/  
  
app.post('/getprofile', function(req, res){
  function getprofile(){
    return new Promise(function(resolve, reject){
      user.getUserById(_.get(req, 'body.user_id', ''), function(err, response){
        if(err)
        {
          reject(null, new Error(err));
        }
        else
        {
          if(_.isEmpty(response))
          {
            reject('profile', null)
          }
          else{
            resolve(response);
          }
        }
      })
    })
  }
  if(validator.isEmpty(_.get(req, 'body.user_id', '')))
  {
    customValidationMsg('User Id is required.', res);
  }else{
    getprofile()
    .then(function(response){
      var data = {detail: response, token:'100101', status : 1, message : 'Profile fetched successfully'};
      var json_arr  = {data: data, success: true};
      res.contentType('application/json');
      res.end(JSON.stringify(json_arr));
    
    })
    .catch(function(derr, err){
      if(derr == 'profile')
      {
        var data = {status : 0, error: err , message : 'Sorry! This user is not registered with us'};
        var json_arr  = {data: data, success: false};
      }
      else{
        var data = {status : 0, error: err , message : 'Oops!! An error occured while updating details.Please try again'};
        var json_arr  = {data: data, success: false};
      }
      
      res.contentType('application/json');
      res.end(JSON.stringify(json_arr));
    })
  } 
})
  
}