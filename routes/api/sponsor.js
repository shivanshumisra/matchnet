exports = module.exports = function(app, sponsor, multer, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, cryptr, _){
const upload = multer({ dest: 'public/uploads/games' });
app.post('/api/getSponsors', function(req, res){
  function getSponsorData(){
    let condData = {};
    let fields = {'name': true,'status': true,'created_on': true};
    return new Promise(function(resolve, reject){
      sponsor.getAllSponsors(condData, fields, function(err, response){
        if(err)
        {
          reject(null, new Error(err));
        }
        else
        {
          resolve(response);
        }
      })
    })
  }
  getSponsorData()
  .then(function(response){
    var json_arr  = {data: ((response.sponsors) ? response.sponsors : []), success: true, message : 'sponsors fetched successfully'};
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
  })
  .catch(function(derr, err){
    var resdata = {status : 0, error: err , pages:''};
    var json_arr  = {sponsors: resdata, success: false, message : 'Oops!! An error occured while updating details.Please try again'};
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
  }) 
})

const sp_fields = [{
     name: 'logo', maxCount: 1
   }, {
     name: 'banner_ads_1', maxCount: 1
   },{
     name: 'banner_ads_2', maxCount: 1
   },{
     name: 'banner_ads_3', maxCount: 1
   }];
app.post('/api/saveSponsor', upload.fields(sp_fields), function(req, res, next){
    var data = {};
    var isError = false;
    var condData = {};
    console.log('req', req.body.name)
    if(validator.isEmpty(_.get(req, 'body.name')))
    {
      data = {detail: []};
      res.render('sponsors',{data: data, status : false, message:'', pages:'', error: 'Name is required.', page:'Sponsor', title: 'Sponsor Management'});
      isError = true;
    }
    function checkSponsor(){
      return new Promise(function(resolve, reject){
        sponsor.getSponsorByName(_.get(req, 'body.name'), function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(!_.isEmpty(response))
            {
              reject('name', null)
            }
            else{
              resolve(response)
            }
          }
        })
      })
    }
    function createSponsor(){
      return new Promise(function(resolve, reject){
        var sponsorData = {
          name: req.body.name,
          description: req.body.description,
          status: req.body.status || 'active',
        };
        const filedata = req.files;
        if(filedata.logo && filedata.logo[0].filename)
        {
          sponsorData.logo = filedata.logo[0].filename;
        }
        if(filedata.banner_ads_1 && filedata.banner_ads_1[0].filename)
        {
          sponsorData.banner_ads_1 = filedata.banner_ads_1[0].filename;
        }
        if(filedata.banner_ads_2 && filedata.banner_ads_2[0].filename)
        {
          sponsorData.banner_ads_2 = filedata.banner_ads_2[0].filename;
        }
        if(filedata.banner_ads_3 && filedata.banner_ads_3[0].filename)
        {
          sponsorData.banner_ads_3 = filedata.banner_ads_3[0].filename;
        }
        sponsor.createData(sponsorData, function(err, response){
          if(err)
          {
              reject('noRecord', new Error(err));
          }
          else
          {
            resolve(response)
          }
        })
      })
    }
    if(!isError){
      checkSponsor()
      .then(function(response){
        console.log('response', response, req.body)
        createSponsor()
        .then(function(resp){
          if(!_.isEmpty(_.get(resp, 'logo'))){
            fs.rename('public/uploads/games/'+resp.logo, 'public/uploads/sponsors/'+resp.logo, function(err) {
              if (!err) {
                fs.unlink('public/uploads/games/'+resp.logo, function(err, response){});
              }
            });
          }
          if(!_.isEmpty(_.get(resp, 'banner_ads_1'))){
            fs.rename('public/uploads/games/'+resp.banner_ads_1, 'public/uploads/sponsors/'+resp.banner_ads_1, function(err) {
              if (!err) {
                fs.unlink('public/uploads/games/'+resp.banner_ads_1, function(err, response){});
              }
            });
          }
          if(!_.isEmpty(_.get(resp, 'banner_ads_2'))){
            fs.rename('public/uploads/games/'+resp.banner_ads_2, 'public/uploads/sponsors/'+resp.banner_ads_2, function(err) {
              if (!err) {
                fs.unlink('public/uploads/games/'+resp.banner_ads_2, function(err, response){});
              }
            });
          }
          if(!_.isEmpty(_.get(resp, 'banner_ads_3'))){
            fs.rename('public/uploads/games/'+resp.banner_ads_3, 'public/uploads/sponsors/'+resp.banner_ads_3, function(err) {
              if (!err) {
                fs.unlink('public/uploads/games/'+resp.banner_ads_3, function(err, response){});
              }
            });
          }
            data = {data: resp, pages:'', status : true, message : 'sponsors fetched successfully', error:'', page:'Game', title: 'Game Management'}
            res.contentType('application/json');
            res.end(JSON.stringify(data));
        })
        .catch(function(derr,err){
          data = {detail: err  , derr: derr,  status : false};
          res.contentType('application/json');
          res.end(JSON.stringify(data));
        })  
      })
      .catch(function(derr,err){
        if(derr == 'name')
        {
          data = {status : false,error: 'Sorry! name already exists.', message : ''};
        }
        else{
          data = {status : false, error: 'Oops!! An error occured while processing request.Please try again' , message : ''};
        }
        res.contentType('application/json');
        res.end(JSON.stringify(data));
      })
    }
  })
//==========================================================================================================================================//

}