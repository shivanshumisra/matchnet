exports = module.exports = function(app, game, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, cryptr, _){

app.post('/api/getGames', function(req, res){
  function getGameData(){
    let condData = {};
    let fields = {'name': true,'status': true,'created_on': true};
    return new Promise(function(resolve, reject){
      game.getAllGames(condData, fields, function(err, response){
        if(err)
        {
          reject(null, new Error(err));
        }
        else
        {
          resolve(response);
        }
      })
    })
  }
  getGameData()
  .then(function(response){
    var json_arr  = {data: ((response.games) ? response.games : []), success: true, message : 'games fetched successfully'};
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
  })
  .catch(function(derr, err){
    var resdata = {status : 0, error: err , pages:''};
    var json_arr  = {games: resdata, success: false, message : 'Oops!! An error occured while updating details.Please try again'};
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
  }) 
})

//==========================================================================================================================================//

}