exports = module.exports = function(app, TronWeb, revealCommit){
	 	// account for croupier
		const tronWeb = new TronWeb({
		    fullNode: 'https://api.shasta.trongrid.io',
		    solidityNode: 'https://api.shasta.trongrid.io',
		    eventServer: 'https://api.shasta.trongrid.io',
		    //croupier private key
		    privateKey: process.env.croupier_private_key
		})

		//contract from croupier account
		//smartconrtract address
		let contract;
		tronWeb.contract().at(process.env.smart_contract_address).then((con) => {
		    contract = con;
		    watchEvent();

		})

	// watch multiple events
	function watchEvent() {
		    //watch for commit event and settle the bet by croupier
		    // back end function //
		    contract.Commit().watch((err, event) => {
		        if (err) return console.error('Error with "method" event:', err);
		        if (event) {
		            console.log(event);
		            tronWeb.trx.getBlock(event.block).then((bl) => {
		                let blockHash = "0x" + bl.blockID;
		                revealCommit.getRevealCommits(event.result.commit, (err, response)=>{
		                	if(err){
		                		console.log('Error : ', err)
		                	}
		                	else{
		                		console.log('data : ', response);
		                		contract.settleBet((response ? response.reveal : null), blockHash).send().then(console.log('Reveal Sent successfully'));
		                	}
		                })
		            })
		        }
		    })

	    //watch for payment events
	    // back end fucntion
	    contract.Payment().watch((err, event) => {
	        if (err) return console.error('Error with "method" event:', err);
	        if (event) { // some function
	            console.log(event);
	        }
	    })

	    // watch for result event with all the params
	    // this fucntion gives us the result of each bet , this includes history also
	    // we can use it in front and back end too for getting the real time data
	    contract.Result().watch((err, event) => {
	        if (err) return console.error('Error with "method" event:', err);
	        if (event) { // some function
	            console.log(event);
	        }
	    })
	}

	

}