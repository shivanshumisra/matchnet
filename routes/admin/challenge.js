exports = module.exports = jsUcfirst = function(string){
  return string.charAt(0).toUpperCase() + string.slice(1);
}

exports = module.exports = function(app, user, challenge, multer, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, io, _){
  const perPage = 15;
  app.get('/admin/challenges/:page?', isAuthenticated, function(req, res, next){
    function getChallenges(page, perPage, condData){
      return new Promise(function(resolve, reject){
        challenge.getChallengeData(page, perPage, condData, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            resolve(response);
          }
        })
      })
    }
    var page = req.params.page || 1;
    var condData = {};
    let perPage = 10000;
    getChallenges(page, perPage, condData).then(function(response) {
      var data = {challenges: response.challenge, status : true, };
      res.render('challenges',{data: data, pages: response.pages, current: response.current,  message : '', error:'', page: 'Challege', title: 'Challege Management'});
    })
    .catch(function(derr, err){
      var data = {challenges: [], status : false};
      res.render('challenges',{data: data, pages:'', error:'Oops!! An error occured while updating details.Please try again', message:'', page:'Challege', title: 'Challege Management'});
    }) 
  })

  app.get('/admin/challenge/:challengeid/detail', isAuthenticated, function(req, res, next){
    var challengeid = req.params.challengeid || null;
    function getChallengeById() {
      return new Promise(function(resolve, reject){
        challenge.getChallengeById(challengeid, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            resolve(response);
          }
        })
      })
    }
    function getCreatorById(userid) {
      return new Promise(function(resolve, reject){
        user.getUserById(userid, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            resolve(response);
          }
        })
      })
    }
    if(challengeid){
      getChallengeById().then(function(response) {
        getCreatorById(response.creator).then(function(resp) {
          var data = {challenge: response, owner : resp , status : true, };
          res.render('view_challenge',{data: data, challengeid:challengeid, message : '', error:'', page: 'Challege', title: 'Challege Management'});
        })
        .catch(function(derr, err){
          res.redirect('/admin/challenges');
        })
        
      })
      .catch(function(derr, err){
        res.redirect('/admin/challenges');
      })  
    }
    else
    {
      res.redirect('/admin/challenges');
    } 
  })
}