exports = module.exports = jsUcfirst = function(string){
  return string.charAt(0).toUpperCase() + string.slice(1);
}

exports = module.exports = function(app, user, game, sponsor, point, tournament, bet, multer, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, io, _){
  const upload = multer({ dest: 'public/uploads/tournament' });
  const perPage = 15;
  var gameServices = require("../../services/games.js")(app, game, sponsor, tournament);
  var prizeMatrix = require('../../utils/prizeMatrix');

  app.get('/admin/tournaments/:page?', isAuthenticated, function(req, res, next){
    var page = req.params.page || 1;
    var condData = {};
    let perPage = 10000;
    var sponsorsData =  gameServices.getSponsors(perPage, page, condData);
    var gamesData =  gameServices.getGames(perPage, page, condData);
    var tournamentsData = gameServices.getTournaments(perPage, page, condData);
    Promise.all([sponsorsData, gamesData, tournamentsData]).then(function(response) {
      var data = {tournaments: response[2].tournaments, games: response[1].games, sponsors: response[0].sponsors, status : true, };
      res.render('tournaments',{data: data, pages: response.pages, current: response.current,  message : '', error:'', page: 'Tournament', title: 'Tournament Management'});
    })
    .catch(function(derr, err){
      var data = {status : false};
      res.render('tournaments',{data: data, pages:'', error:'Oops!! An error occured while updating details.Please try again', message:'', page:'Tournament', title: 'Tournament Management'});
    }) 
  })

  app.get('/admin/tournament/edit/:tournamentid', isAuthenticated, function(req, res, next){
    var tournamentid = req.params.tournamentid || null;
    if(tournamentid){
      var page = req.params.page || 1;
      var condData = {};
      let perPage = 10000;
      var sponsorsData =  gameServices.getSponsors(perPage, page, condData);
      var gamesData =  gameServices.getGames(perPage, page, condData);
      var tournamentData = gameServices.getTournamentById(tournamentid);
      Promise.all([sponsorsData, gamesData, tournamentData]).then(function(response) {
        if(response[2].startdate){
          let start_datetime = response[2].startdate;
          response[2].startdate = (response[2].startdate) ? MOMENT(start_datetime).format("YYYY-MM-DD HH:mm:ss") : null;
        }
        if(response[2].enddate){
          let end_datetime = response[2].enddate;
          response[2].enddate = (response[2].enddate) ? MOMENT(end_datetime).format("YYYY-MM-DD HH:mm:ss") : null;
        }
        var data = {detail: response[2], games: response[1].games, sponsors: response[0].sponsors, status : true, };
        res.render('tournaments',{data: data, tournamentid:tournamentid, message : '', error:'', page: 'Tournament', title: 'Tournament Management'});
      })
      .catch(function(derr, err){
        var data = {status : false};
        res.render('tournaments',{data: data, tournamentid:tournamentid, error:'Oops!! An error occured while updating details.Please try again', message:'', page:'Tournament', title: 'Tournament Management'});
      })  
    }
    else
    {
      res.redirect('/admin/tournaments');
    } 
  })

  app.post('/admin/tournaments/:page?', isAuthenticated, upload.single('logo'), function(req, res, next){
    
    var data = {detail: []};
    var isError = false;
    var page = req.params.page || 1;
    var condData = {};
    var tmpErr = '';
    if(validator.isEmpty(_.get(req, 'body.name')))
    {
      tmpErr = 'Name is required.';
      isError = true;
    }
    if(validator.isEmpty(_.get(req, 'body.gameid')))
    {
      tmpErr = 'Game is required.';
      isError = true;
    }
    if(validator.isEmpty(_.get(req, 'body.noOfPlayer')))
    {
      tmpErr = 'NO Of Players is required.';
      isError = true;
    }
    if(validator.isEmpty(_.get(req, 'body.sponsorid')))
    {
      tmpErr = 'Sponsor is required.';
      isError = true;
    }
    if(isError){
      res.render('tournaments',{data: data, status : false, message:'', pages:'', error: tmpErr, page:'Game', title: 'Game Management'});
    }
      
    function checkTournament(){
      return new Promise(function(resolve, reject){
        tournament.getTournamentByName(_.get(req, 'body.name'), function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(!_.isEmpty(response))
            {
              reject('name', null)
            }
            else{
              resolve(response, '')
            }
          }
        })
      })
    }
    function createTournament(){
      return new Promise(function(resolve, reject){
        var tournamentData = {
          name: req.body.name,
          gameid: req.body.gameid,
          manual: (req.body.manual == 'on') ? true : false,
          noOfPlayer: req.body.noOfPlayer,
          winning_amount: (req.body.winning_amount) ? req.body.winning_amount : 0,
          status: req.body.status || 'active',
        };
        if(req.body.description){
          tournamentData.description = req.body.description;
        }
        if(req.body.entry_fee){
          tournamentData.entry_fee = req.body.entry_fee;
        }
        if(req.body.startdate){
          tournamentData.startdate = new Date(req.body.startdate).toUTCString();
        }
        if(req.body.enddate){
          tournamentData.enddate = new Date(req.body.enddate).toUTCString();
        }
        if(req.body.sponsorid){
          tournamentData.sponsorid = req.body.sponsorid;
        }
        const filedata = req.file;
        if(filedata)
        {
          tournamentData.logo = filedata.filename;
        }
        if(req.body.status == 'Ongoing' && req.body.manual == 'on'){
          tournamentData.startdate = new Date().toUTCString();
        }
        
        tournament.createData(tournamentData, function(err, response){
          if(err)
          {
              reject('noRecord', new Error(err));
          }
          else
          {
            resolve(response)
          }
        })
      })
    }
    function sendPublishTournament(no_of_tournament, name, status) {
      global.io.emit("tournament_published", {no_of_tournament: no_of_tournament, status:status, details:[name], message: "tournament published"});
    }
    function getTournamentData(etype = '', msg = ''){
      var page = req.params.page || 1;
      var condData = {};
      var sponsorsData =  gameServices.getSponsors(perPage, page, condData);
      var gamesData =  gameServices.getGames(perPage, page, condData);
      var tournamentsData = gameServices.getTournaments(perPage, page, condData);
      Promise.all([sponsorsData, gamesData, tournamentsData]).then(function(response) {
        let tmpmsg = (etype == 'success') ? msg : '';
        let tmperr = (etype == 'error') ? msg : '';
        var data = {tournaments: response[2].tournaments, games: response[1].games, sponsors: response[0].sponsors, status : true, };
        res.render('tournaments',{data: data, pages: response.pages, current: response.current,  message : tmpmsg, error: tmperr, page: 'Tournament', title: 'Tournament Management'});
      })
      .catch(function(derr, err){
        var data = {status : false};
        res.render('tournaments',{data: data, pages:'', error:'Oops!! An error occured while updating details.Please try again', message:'', page:'Tournament', title: 'Tournament Management'});
      }) 
    }
    if(!isError){
      checkTournament()
      .then(function(response){
        createTournament()
        .then(function(resp){
          if(resp.status == 'Ongoing' || resp.status == 'Upcoming'){
            sendPublishTournament(1, req.body.name, resp.status);
          }
          getTournamentData('success', 'Tournament has been created successfully')
        })
        .catch(function(derr,err){
          getTournamentData()
        })  
      })
      .catch(function(derr,err){
        let tmperr = '';
        if(derr == 'name')
        {
          tmperr = 'Sorry! name already exists.';
        }
        else{
          tmperr = 'Oops!! An error occured while processing request.Please try again';
        }
        getTournamentData('error', tmperr)
      })
    }
  })

  app.post('/admin/tournament/edit/:tournamentid', isAuthenticated, upload.single('logo'), function(req, res, next){
    var tournamentid = req.params.tournamentid || null;
    var isError = false;
    if(!tournamentid){
      res.redirect('/admin/tournaments');
    }

    var tmpErr = '';
    if(validator.isEmpty(_.get(req, 'body.name')))
    {
      tmpErr = 'Name is required.';
      isError = true;
    }
    if(validator.isEmpty(_.get(req, 'body.gameid')))
    {
      tmpErr = 'Game is required.';
      isError = true;
    }
    if(validator.isEmpty(_.get(req, 'body.sponsorid')))
    {
      tmpErr = 'Sponsor is required.';
      isError = true;
    }
    if(isError){
      res.render('tournaments',{data: [], tournamentid:tournamentid, status : false, message:'', pages:'', error: tmpErr, page:'Tournament', title: 'Tournament Management'});
    }
    function checkTournament(tournamentid){
      return new Promise(function(resolve, reject){
        tournament.getTournamentByName(_.get(req, 'body.name'), function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(!_.isEmpty(response) && response._id != tournamentid)
            {
              reject('name', null)
            }
            else{
              resolve(response)
            }
          }
        })
      })
    }
    function updateTournament(tournamentId){
      return new Promise(function(resolve, reject){
        var tournamentData = {
          name: req.body.name,
          gameid: req.body.gameid,
          noOfPlayer: req.body.noOfPlayer,
          winning_amount: (req.body.winning_amount) ? req.body.winning_amount : 0,
          manual: (req.body.manual == 'on') ? true : false,
          status: req.body.status || 'active',
        };
        if(req.body.description){
          tournamentData.description = req.body.description;
        }
        if(req.body.entry_fee){
          tournamentData.entry_fee = req.body.entry_fee;
        }
        if(req.body.startdate){
          tournamentData.startdate = new Date(req.body.startdate).toUTCString();
        }
        if(req.body.enddate){
          tournamentData.enddate = new Date(req.body.enddate).toUTCString();
        }
        if(req.body.sponsorid){
          tournamentData.sponsorid = req.body.sponsorid;
        }
        const filedata = req.file;
        if(filedata)
        {
          tournamentData.logo = filedata.filename;
        }
        if(req.body.status == 'Ongoing' && req.body.manual == 'on'){
          tournamentData.startdate = new Date().toUTCString();
        }
        if(req.body.status == 'Finished'){
          tournamentData.enddate = new Date().toUTCString();
        }
        tournament.updateTournament(tournamentId, tournamentData, function(err, response){
          if(err)
          {
              reject('noRecord', new Error(err));
          }
          else
          {
            resolve(response)
          }
        })
      })
    }
    function getTournamentData(etype = '', msg = '', tournamentid){
      var page = req.params.page || 1;
      var condData = {};
      let perPage = 100000;
      var sponsorsData =  gameServices.getSponsors(perPage, page, condData);
      var gamesData =  gameServices.getGames(perPage, page, condData);
      var tournamentData = gameServices.getTournamentById(tournamentid);
      Promise.all([sponsorsData, gamesData, tournamentData]).then(function(response) {
        let tmpmsg = (etype == 'success') ? msg : '';
        let tmperr = (etype == 'error') ? msg : '';
        // if(response[2].startdate){
        //   response[2].startdate = new Date(response[2].startdate).toDateString();
        //   response[2].enddate = new Date(response[2].enddate).toDateString();
        // }
        if(response[2].startdate){
          let start_datetime = response[2].startdate;
          response[2].startdate = (response[2].startdate) ? MOMENT(start_datetime).format("YYYY-MM-DD HH:mm:ss") : null;
        }
        if(response[2].enddate){
          let end_datetime = response[2].enddate;
          response[2].enddate = (response[2].enddate) ? MOMENT(end_datetime).format("YYYY-MM-DD HH:mm:ss") : null;
        }
        var data = {detail: response[2], games: response[1].games, sponsors: response[0].sponsors, status : true, };
        res.render('tournaments',{data: data, pages: response.pages, tournamentid:tournamentid, current: response.current,  message : tmpmsg, error: tmperr, page: 'Tournament', title: 'Tournament Management'});
      })
      .catch(function(derr, err){
        var data = {status : false};
        res.render('tournaments',{data: data, tournamentid:tournamentid, pages:'', error:'Oops!! An error occured while updating details.Please try again', message:'', page:'Tournament', title: 'Tournament Management'});
      }) 
    }
    function sendPublishTournament(no_of_tournament, name, status) {
      global.io.emit("tournament_published", {no_of_tournament: no_of_tournament, status: status, details:[name], message: "tournament published"});
    }
    if(!isError){
      checkTournament(tournamentid)
      .then(function(response){
        updateTournament(tournamentid)
        .then(function(resp){
          if(!_.isEmpty(_.get(resp, 'logo'))){
            if(response.logo && response.logo !=''){
              fs.unlink('public/uploads/tournaments/'+response.logo, function(err, response){});
            }
          }
          if(resp.status == 'Ongoing'){
            sendPublishTournament(1, response.name, resp.status)
          }
          if(resp.status == 'Finished'){
            global.io.emit("tournament_finished", {data:response, message: "tournament finished"});
          }
          getTournamentData('success', 'Record Updated successfully', tournamentid)
        })
        .catch(function(derr,err){
          getTournamentData('error', 'Oops!! An error occured while processing request.Please try again', tournamentid)
        })  
      })
      .catch(function(derr,err){
        let tmpdata ='';
        if(derr == 'name')
        {
          tmpdata = 'Sorry! name already exists.';
        }
        else{
          tmpdata = 'Oops!! An error occured while processing request.Please try again';
        }
        getTournamentData('error', tmpdata, tournamentid)
      })
    }
  })

  app.get('/admin/tournament/delete/:tournamentid', isAuthenticated, function(req, res, next){
    var tournamentid = req.params.tournamentid || null;
    function getTournamentById(){
      return new Promise(function(resolve, reject){
        tournament.getTournamentById(tournamentid, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(_.isEmpty(response))
            {
              reject('tournamentNotFound', null)
            }
            else{
              resolve(response);
            }
          }
        })
      })
    }
    function removeTournamentById(){
      return new Promise(function(resolve, reject){
        tournament.removeTournamentById(tournamentid, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            resolve(response);
          }
        })
      })
    }
    if(tournamentid){
      getTournamentById()
      .then(function(response){
        
        removeTournamentById()
        .then(function(resp){  
          if(!_.isEmpty(_.get(response, 'logo'))){
            fs.unlink('public/uploads/tournament/'+response.logo, function(err, response){
            });
          }
          res.redirect('/admin/tournaments');   
        })
        .catch(function(derr, err){
          res.redirect('/admin/tournaments');
        })
      })
      .catch(function(derr, err){
        res.redirect('/admin/tournaments');
      }) 
    }
    else
    {
      res.redirect('/admin/tournaments');
    }
  })

  app.get('/admin/tournament/start/:tournamentid', isAuthenticated, function(req, res, next){
    var tournamentid = req.params.tournamentid || null;
    function updateTournament(tournamentId){
      return new Promise(function(resolve, reject){
        var tournamentData = {
          startdate: new Date().toUTCString(),
          status: 'Ongoing',
        };
        tournament.updateTournament(tournamentId, tournamentData, function(err, response){
          if(err)
          {
              reject('noRecord', new Error(err));
          }
          else
          {
            resolve(response)
          }
        })
      })
    }

    function sendPublishTournament(no_of_tournament, name, status) {
      global.io.emit("tournament_published", {no_of_tournament: no_of_tournament, status: status, details: [name], message: "tournament published"});
    }
    if(tournamentid){
      updateTournament(tournamentid)
      .then(function(response){
        sendPublishTournament(1, response.name, 'Ongoing');
        res.redirect('/admin/tournaments'); 
      })
      .catch(function(derr, err){
        res.redirect('/admin/tournaments');
      }) 
    }
    else
    {
      res.redirect('/admin/tournaments');
    }
  })

  app.get('/admin/tournament/stop/:tournamentid', isAuthenticated, function(req, res, next){
    var tournamentid = req.params.tournamentid || null;
    function saveWinner(tournamentId, playerIds) {
      return new Promise(function(resolve, reject){
        tournament.updateWinner(tournamentId, playerIds, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
              resolve(response);
            }
        })
      })
    }
    function getBetDetailByTournamentId(tournamentId){
      return new Promise(function(resolve, reject){    
        bet.getBetsByTournamentId(tournamentId, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function getPointDetailByTournamentId(tournamentId){
      return new Promise(function(resolve, reject){    
        point.getMaxPointDetailByTournamentId(tournamentId, function(err, response){
          if(err){
            reject(null, new Error(err));
          }
          else{
            resolve(response);
          }
        })
      })
    }
    function sendFinishedTournament(response) {
      global.io.emit("tournament_finished", {data:response, message: "tournament finished"});
    }

    function calculateWinnerAmount(winPlayer, totalWinPlayer, playerArr, noOfPlayer, enrolledPlayer, winningAmount, entryFee){
      let prizeAmt = 0;
      if(entryFee == 0){
        prizeAmt = winningAmount
      }
      if(entryFee > 0){
        prizeAmt = (enrolledPlayer.length * entryFee)*.95;
      }
      if(entryFee==0){
        let prizeAmt = winningAmount*.95;
      }
      if(winPlayer.length > 0){
        _.forEach(playerArr, function(value, key) {
          let playerWinAmt = prizeAmt * (totalWinPlayer[key]/100);
          playerArr[key].win_amount = Math.round(playerWinAmt);
          playerArr[key].win_percentage = totalWinPlayer[key];
          playerArr[key].user_position = (key + 1);
        })
      }
      return playerArr;
    }
    if(tournamentid){
      var getPointDetailByTournamentId = getPointDetailByTournamentId(tournamentid);
      var tournamentData = gameServices.getTournamentById(tournamentid);
      Promise.all([getPointDetailByTournamentId, tournamentData]).then(function(response) {
        // let getWin = _.find(response[0], (point) => (point && point.result == 'win')); 
        let getWinIds = [];
        let totalWinPlayer = [];
        if(response[1].noOfPlayer > 0){
          totalWinPlayer = prizeMatrix.calculatePrize(response[1].noOfPlayer);

          if(response[0].length > 0){
            let tmpWinArr = [];

            if(response[0].length > totalWinPlayer.length){
              tmpWinArr = response[0].slice(totalWinPlayer.length);
            }
            else
            {
              tmpWinArr = response[0];
            }

            if(tmpWinArr.length > 0){
              getWinIds = tmpWinArr.map(player => player.playerId);
            }
          }
        }
        
        //calculate winner
        //update status - start
        // if(getWin && getWin.playerId){
          let winPlayer = (getWinIds && getWinIds.length > 0) ? getWinIds : [];
          let tmpresponse = (winPlayer) ? response[0] : response[1];
          let noOfPlayer = response[1].noOfPlayer;
          let enrolledPlayer = response[1].players;
          let winningAmount = response[1].winning_amount;
          let entryFee = response[1].entry_fee;
          if(winPlayer.length > 0){
            winPlayer = calculateWinnerAmount(winPlayer, totalWinPlayer, tmpresponse, noOfPlayer, enrolledPlayer, winningAmount, entryFee);
          }
          
          saveWinner(tournamentid, winPlayer)
          .then(function(resp){
            sendFinishedTournament(tmpresponse);
            res.redirect('/admin/tournaments'); 
          })
          .catch(function(derr, err){
            res.redirect('/admin/tournaments');
          })
        // }
        // else {
        //   res.redirect('/admin/tournaments');
        // }
        //update status - end
      })
      .catch(function(derr, err){
        res.redirect('/admin/tournaments');
      }) 
    }
    else
    {
      res.redirect('/admin/tournaments');
    }
  })

  app.get('/admin/tournament/:tournamentid/detail', isAuthenticated, function(req, res, next){
    var tournamentid = req.params.tournamentid || null;
    function getTournamentById() {
      return new Promise(function(resolve, reject){
        tournament.getTournamentDetailById(tournamentid, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            resolve(response);
          }
        })
      })
    }
    function getBetsByTournamentId(tournamentid) {
      return new Promise(function(resolve, reject){
        bet.getBetsByTournamentId(tournamentid, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            resolve(response);
          }
        })
      })
    }
    if(tournamentid){
      getTournamentById().then(function(response) {
        
        getBetsByTournamentId(response._id).then(function(resp) {
          var data = {tournament: response, bets : resp , status : true, };
          res.render('view_tournament',{data: data, tournamentid:tournamentid, message : '', error:'', page: 'Challege', title: 'Challege Management'});
        })
        .catch(function(derr, err){
          res.redirect('/admin/tournaments');
        })
      })
      .catch(function(derr, err){
        res.redirect('/admin/tournaments');
      })  
    }
    else
    {
      res.redirect('/admin/tournaments');
    } 
  })
}