exports = module.exports = function(app, user, game, sponsor, bet, multer, nodemailer, smtpTransport, passwordHash, customValidationMsg, validator, waterfall, encryptor, mongoose, fs, path, jwt, MOMENT, otpGenerator, isAuthenticated, _){
  const upload = multer({ dest: 'public/uploads/games' });
  const perPage = 15;
  var gameServices = require("../../services/games.js")(app, game, sponsor);
  
  app.get('/admin/login', function(req, res, next){
    var sess = req.session; 
    if(_.get(sess, 'userId') && sess.userId !='' ){
      res.redirect('/admin/users');
    }
    else{
      res.render('login',{page:'Login', title:'Login', message : '', error : '', });
    }
    
  })

  app.post('/admin/login', function(req, res, next){
    function signin(){
      console.log('req', req.body);
      return new Promise(function(resolve, reject){
        user.getUserByUsername(_.get(req, 'body.username'), function(err, response){
            if(err)
            {
                reject('noRecord', new Error(err));
            }
            else
            {
            if(_.isEmpty(response))
            {
              reject('username', null)
            }
            else if(!response.password)
            {
              reject('invalid', null)
            }
            else if(passwordHash.verify(_.get(req, 'body.password'), _.get(response, 'password', '')) && _.get(response, 'user_type', '') =='admin')
            {
              resolve(response)
            }
            else{
              reject('invalid', null)
            }
            }
        })
      })
    }
    if(validator.isEmpty(_.get(req, 'body.username', '')))
    {
      var data = {status : false, error: 'username is required.', message : '', page:'Login', title: 'User Login'};
      res.render('login',data);
    }
    else if(validator.isEmpty(_.get(req, 'body.password')))
    {
      var data = {status : false, error: 'Password is required.', message : '', page:'Login', title: 'User Login'};
      res.render('login', data);
    }
    else{
      signin()
      .then(function(response){
          const payload = {
            userid: response._id   
          };
          var token = jwt.sign(payload, process.env.key, {
            expiresIn: process.env.tokenexp
          });
          var sess = req.session; 
          var data = {detail: response, token:token, status :true, message : 'You have Logged-in successfully.'};
          var json_arr  = {data: data, success: true};
          req.session.userId = response._id;
          req.session.user = response;
          res.redirect('/admin/users');
      })
      .catch(function(derr,err){
        if(derr == 'username' || derr == 'noRecord')
        {
          var data = {detail: {}, status : false, error : 'Sorry! You are not registered with us.', message : '', page:'Login', title: 'User Login'};
        }
        else if(derr == 'invalid')
        {
          var data = {status : false, error: 'Invalid Credentials.' , details:'', page:'Login', message : '', title: 'User Login'};
          var json_arr  = {data: data, success: false};
        }
        else if(derr == 'required')
        {
          var data = {status : false, error: err , details:'', message : '', page:'Login', title: 'User Login'};
        }
        else{
          var data = {status : false, error: 'Oops!! An error occured while processing request.Please try again', message : '', page:'Login', title: 'User Login'};
        }
        req.flash('error', data.error);
        res.render('login',data);
      })
    } 
  })

  app.get('/admin/users/:page?', isAuthenticated, function(req, res, next){
    var page = req.params.page || 1
    let condData = {user_type: {'$ne':'admin'}, $or:[{is_deleted: { '$exists': true, '$ne': 1 }},{is_deleted: { '$exists': false}}]},
    perPage = 10;
   
    function getUserData(){
      return new Promise(function(resolve, reject){
        user.getUserData(page, perPage, condData, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(_.isEmpty(response))
            {
              reject('userList', null)
            }
            else{
              resolve(response);
            }
          }
        })
      })
    }
    getUserData()
    .then(function(response){
      var data = {detail: response.users, status : true, message : 'users fetched successfully'};
      res.render('userslist',{data: data, pages: response.pages, current: response.current,  page: 'userList', title: 'User Listing'});
    
    })
    .catch(function(derr, err){
      if(derr == 'userList')
      {
        var data = {status : false, error: err , pages:'', message : 'Sorry! This user is not registered with us'};
        var json_arr  = {data: data, success: false};
      }
      else{
        var data = {status : false, error: err , pages:'', message : 'Oops!! An error occured while updating details.Please try again'};
        var json_arr  = {data: data, success: false};
      }
      res.render('userslist',{data: data, pages:'', page:'userList', title: 'User Listing'});
    }) 
  })

  app.get('/admin/user/delete/:userid', isAuthenticated, function(req, res, next){
    var userid = req.params.userid || null;
    function getUserById(){
      return new Promise(function(resolve, reject){
        user.getUserById(userid, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(_.isEmpty(response))
            {
              reject('userNotFound', null)
            }
            else{
              resolve(response);
            }
          }
        })
      })
    }
    function removeUserById(){
      return new Promise(function(resolve, reject){
        user.update_commonById(userid, {'is_deleted': 1}, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            resolve(response);
          }
        })
      })
    }
    if(userid){
      getUserById()
      .then(function(response){
        
        removeUserById()
        .then(function(resp){  
          res.redirect('/admin/users');   
        })
        .catch(function(derr, err){
          res.redirect('/admin/users');
        })
      })
      .catch(function(derr, err){
        res.redirect('/admin/users');
      }) 
    }
    else
    {
      res.redirect('/admin/users');
    }
  })

  app.get('/admin/user/:userid/detail', isAuthenticated, function(req, res, next){
    var userid = req.params.userid || null
    if(userid){
      function getUserData(){
        return new Promise(function(resolve, reject){
          user.getUserById(userid, function(err, response){
            if(err)
            {
              reject(null, new Error(err));
            }
            else
            {
              if(_.isEmpty(response))
              {
                reject('userData', null)
              }
              else{
                resolve(response);
              }
            }
          })
        })
      }
      getUserData()
      .then(function(response){
        var data = {detail: response, status : true};
        res.render('userdetail',{data: data, page: 'userDetail', title: 'User Details'});
      })
      .catch(function(derr, err){
        if(derr == 'userData')
        {
          var data = {status : false, error: err , pages:'', message : 'Sorry! This user is not registered with us'};
          var json_arr  = {data: data, success: false};
        }
        else{
          var data = {status : false, error: err , pages:'', message : 'Oops!! An error occured while updating details.Please try again'};
          var json_arr  = {data: data, success: false};
        }
        res.render('userdetail',{data: data, pages:'', page:'userDetail', title: 'User Details'});
      })
    }
    else{
      res.redirect('/admin/users');
    }
  })

  app.get('/admin/games/:page?', isAuthenticated, function(req, res, next){
    var page = req.params.page || 1;
    var condData = {};
    gameServices.getGames(perPage, page, condData).then(function(response){
      var data = {detail: response.games, status : true, };
      res.render('games',{data: data, pages: response.pages, current: response.current,  message : '', error:'', page: 'Game', title: 'Game Management'});
    
    })
    .catch(function(derr, err){
      var data = {status : false};
      res.render('games',{data: data, pages:'', error:'Oops!! An error occured while updating details.Please try again', message:'', page:'Game', title: 'Game Management'});
    }) 
  })

  app.get('/admin/game/edit/:gameid', isAuthenticated, function(req, res, next){
    var gameid = req.params.gameid || null;
    if(gameid){
      gameServices.getGameById(gameid).then(function(response){
        var data = {detail: response, status : true, };
        res.render('games',{data: data, gameid:gameid, message : '', error:'', page: 'Game', title: 'Game Management'});
      })
      .catch(function(derr, err){
        var data = {status : false};
        res.render('games',{data: data, pages:'', error:'Oops!! An error occured while updating details.Please try again', message:'', page:'Sponsor', title: 'Sponsor Management'});
      }) 
    }
    else
    {
      res.redirect('/admin/games');
    } 
  })

  app.post('/admin/games/:page?', isAuthenticated, upload.single('logo'), function(req, res, next){
    var data = {};
    var isError = false;
    var page = req.params.page || 1;
    var condData = {};
    if(validator.isEmpty(_.get(req, 'body.name')))
    {
      data = {detail: []};
      res.render('games',{data: data, status : false, message:'', pages:'', error: 'Name is required.', page:'Game', title: 'Game Management'});
      isError = true;
    }
    function checkGame(){
      return new Promise(function(resolve, reject){
        game.getGameByName(_.get(req, 'body.name'), function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(!_.isEmpty(response))
            {
              reject('name', null)
            }
            else{
              resolve(response)
            }
          }
        })
      })
    }
    function createGame(){
      return new Promise(function(resolve, reject){
        var gameData = {
          name: req.body.name,
          description: req.body.description,
          genre: req.body.genre,
          status: req.body.status || 'active',
        };
        const filedata = req.file;
        if(filedata)
        {
          gameData.logo = filedata.filename;
        }
        game.createData(gameData, function(err, response){
          if(err)
          {
              reject('noRecord', new Error(err));
          }
          else
          {
            resolve(response)
          }
        })
      })
    }
    if(!isError){
      checkGame()
      .then(function(response){
        createGame()
        .then(function(resp){
          
          gameServices.getGames(perPage, page, condData).then(function(response){
            data = {detail: response.games};
            res.render('games',{data: data, pages:'', status : true, message : 'Games fetched successfully', error:'', pages: response.pages, current: response.current, page:'Game', title: 'Game Management'});
          })
        })
        .catch(function(derr,err){
          gameServices.getGames(perPage, page, condData).then(function(response){
            data = {detail: response.games,  status : false};
            res.render('games',{data: data, pages:'', page:'Game', title: 'Game Management', error:'Oops!! An error occured while processing request.Please try again'  , message : ''});
          })
        })  
      })
      .catch(function(derr,err){
        let tmperr = '';
        if(derr == 'name')
        {
          tmperr = 'Sorry! name already exists.';
        }
        else{
          tmperr = 'Oops!! An error occured while processing request.Please try again';
        }
        gameServices.getGames(perPage, page, condData).then(function(response){
          let mdata = {detail: response.games};
          res.render('games',{data: mdata, pages: response.pages, current: response.current, page:'Game', title: 'Game Management', error: tmperr, message:''});
        })
      })
    }
  })

  app.post('/admin/game/edit/:gameid', isAuthenticated, upload.single('logo'), function(req, res, next){
    var gameid = req.params.gameid || null;
    var isError = false;
    if(!gameid){
      res.redirect('/admin/games');
    }

    if(validator.isEmpty(_.get(req, 'body.name')))
    {
      data = {detail: []};
      res.render('games',{data: data, status : false, message:'', pages:'', error: 'Name is required.', page:'Game', title: 'Game Management'});
      isError = true;
    }
    function checkGame(gameid){
      return new Promise(function(resolve, reject){
        game.getGameByName(_.get(req, 'body.name'), function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(!_.isEmpty(response) && response._id != gameid)
            {
              reject('name', null)
            }
            else{
              resolve(response)
            }
          }
        })
      })
    }
    function updateGame(gameId){
      return new Promise(function(resolve, reject){
        var gameData = {
          name: req.body.name,
          description: req.body.description,
          status: req.body.status || 'active',
        };
        const filedata = req.file;
        if(filedata)
        {
          gameData.logo = filedata.filename;
        }
        game.updateGame(gameId, gameData, function(err, response){
          if(err)
          {
              reject('noRecord', new Error(err));
          }
          else
          {
            resolve(response)
          }
        })
      })
    }
    if(!isError){
      checkGame(gameid)
      .then(function(response){
        updateGame(gameid)
        .then(function(resp){
          if(!_.isEmpty(_.get(resp, 'logo'))){
            if(response.logo && response.logo !=''){
              fs.unlink('public/uploads/games/'+response.logo, function(err, response){});
            }
          }
          gameServices.getGameById(gameid).then(function(response){
            var data = {detail: response, status : true, };
            res.render('games',{data: data, gameid:gameid, message : 'Record Updated successfully', error:'', page: 'Game', title: 'Game Management'});
          })
        })
        .catch(function(derr,err){
          gameServices.getGameById(gameid).then(function(response){
            data = {detail: response  ,  status : false};
            res.render('games',{data: data, pages:'', page:'Game', title: 'Game Management', message : '', error:'Oops!! An error occured while processing request.Please try again'});
          })
        })  
      })
      .catch(function(derr,err){
        let tmpdata ={};
        if(derr == 'name')
        {
          tmpdata = {status : false,error: 'Sorry! name already exists.', message : ''};
        }
        else{
          tmpdata = {status : false, error: 'Oops!! An error occured while processing request.Please try again' , message : ''};
        }
        gameServices.getGameById(gameid).then(function(response){
          var data = {detail: response, status : true, };
          res.render('games',{...tmpdata , data: data, gameid:gameid, page: 'Game', title: 'Game Management'});
        })
      })
    }
  })

  app.get('/admin/game/delete/:gameid', isAuthenticated, function(req, res, next){
    var gameid = req.params.gameid || null;
    function getGameById(){
      return new Promise(function(resolve, reject){
        game.getGameById(gameid, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(_.isEmpty(response))
            {
              reject('gameNotFound', null)
            }
            else{
              resolve(response);
            }
          }
        })
      })
    }
    function removeGameById(){
      return new Promise(function(resolve, reject){
        game.removeGameById(gameid, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            resolve(response);
          }
        })
      })
    }
    if(gameid){
      getGameById()
      .then(function(response){
        
        removeGameById()
        .then(function(resp){  
          if(!_.isEmpty(_.get(response, 'logo'))){
            fs.unlink('public/uploads/games/'+response.logo, function(err, response){
            });
          }
          res.redirect('/admin/games');   
        })
        .catch(function(derr, err){
          res.redirect('/admin/games');
        })
      })
      .catch(function(derr, err){
        res.redirect('/admin/games');
      }) 
    }
    else
    {
      res.redirect('/admin/games');
    }
  })

  app.get('/admin/sponsors/:page?', isAuthenticated, function(req, res, next){
    var page = req.params.page || 1;
    var condData = {};
    gameServices.getSponsors(perPage, page, condData).then(function(response){
      var data = {detail: response.sponsors, status : true, };
      res.render('sponsors',{data: data, pages: response.pages, current: response.current,  message : '', error:'', page: 'Sponsor', title: 'Sponsor Management'});
    
    })
    .catch(function(derr, err){
      var data = {status : false};
      res.render('sponsors',{data: data, pages:'', error:'Oops!! An error occured while updating details.Please try again', message:'', page:'Sponsor', title: 'Sponsor Management'});
    }) 
  })

  app.get('/admin/sponsor/edit/:sponsorid', isAuthenticated, function(req, res, next){
    var sponsorid = req.params.sponsorid || null;
    if(sponsorid){
      gameServices.getSponsorById(sponsorid).then(function(response){
        var data = {detail: response, status : true, };
        res.render('sponsors',{data: data, sponsorid:sponsorid, message : '', error:'', page: 'Sponsor', title: 'Sponsor Management'});
      })
      .catch(function(derr, err){
        var data = {status : false};
        res.render('sponsors',{data: data, pages:'', error:'Oops!! An error occured while updating details.Please try again', message:'', page:'Sponsor', title: 'Sponsor Management'});
      }) 
    }
    else
    {
      res.redirect('/admin/sponsors');
    }
  })

  const sp_fields = [{
     name: 'logo', maxCount: 1
   }, {
     name: 'banner_ads_1', maxCount: 1
   },{
     name: 'banner_ads_2', maxCount: 1
   },{
     name: 'banner_ads_3', maxCount: 1
   }];

  app.post('/admin/sponsor/edit/:sponsorid', isAuthenticated, upload.fields(sp_fields), function(req, res, next){
    var sponsorid = req.params.sponsorid || null;
    var isError = false;
    if(!sponsorid){
      res.redirect('/admin/sponsors');
    }

    if(validator.isEmpty(_.get(req, 'body.name')))
    {
      data = {detail: []};
      res.render('sponsors',{data: data, status : false, message:'', pages:'', error: 'Name is required.', page:'Sponsor', title: 'Sponsor Management'});
      isError = true;
    }
    function checkSponsor(sponsorid){
      return new Promise(function(resolve, reject){
        sponsor.getSponsorByName(_.get(req, 'body.name'), function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(!_.isEmpty(response) && response._id != sponsorid)
            {
              reject('name', null)
            }
            else{
              resolve(response)
            }
          }
        })
      })
    }
    function updateSponsor(sponsorId){
      return new Promise(function(resolve, reject){
        var sponsorData = {
          name: req.body.name,
          description: req.body.description,
          status: req.body.status || 'active',
        };
        const filedata = req.files;
        if(filedata.logo && filedata.logo[0].filename)
        {
          sponsorData.logo = filedata.logo[0].filename;
        }
        if(filedata.banner_ads_1 && filedata.banner_ads_1[0].filename)
        {
          sponsorData.banner_ads_1 = filedata.banner_ads_1[0].filename;
        }
        if(filedata.banner_ads_2 && filedata.banner_ads_2[0].filename)
        {
          sponsorData.banner_ads_2 = filedata.banner_ads_2[0].filename;
        }
        if(filedata.banner_ads_3 && filedata.banner_ads_3[0].filename)
        {
          sponsorData.banner_ads_3 = filedata.banner_ads_3[0].filename;
        }
        sponsor.updateSponsor(sponsorId, sponsorData, function(err, response){
          if(err)
          {
              reject('noRecord', new Error(err));
          }
          else
          {
            resolve(response)
          }
        })
      })
    }
    if(!isError){
      checkSponsor(sponsorid)
      .then(function(response){
        updateSponsor(sponsorid)
        .then(function(resp){
          if(!_.isEmpty(_.get(resp, 'logo'))){
            fs.rename('public/uploads/games/'+resp.logo, 'public/uploads/sponsors/'+resp.logo, function(err) {
              if (!err) {
                fs.unlink('public/uploads/games/'+resp.logo, function(err, response){});
                if(response.logo && response.logo !=''){
                  fs.unlink('public/uploads/sponsors/'+response.logo, function(err, response){});
                }
              }
            });
          }
          if(!_.isEmpty(_.get(resp, 'banner_ads_1'))){
            fs.rename('public/uploads/games/'+resp.banner_ads_1, 'public/uploads/sponsors/'+resp.banner_ads_1, function(err) {
              if (!err) {
                fs.unlink('public/uploads/games/'+resp.banner_ads_1, function(err, response){});
                if(response.banner_ads_1 && response.banner_ads_1 !=''){
                  fs.unlink('public/uploads/sponsors/'+response.banner_ads_1, function(err, response){});
                }
              }
            });
          }
          if(!_.isEmpty(_.get(resp, 'banner_ads_2'))){
            fs.rename('public/uploads/games/'+resp.banner_ads_2, 'public/uploads/sponsors/'+resp.banner_ads_2, function(err) {
              if (!err) {
                fs.unlink('public/uploads/games/'+resp.banner_ads_2, function(err, response){});
                if(response.banner_ads_2 && response.banner_ads_2 !=''){
                  fs.unlink('public/uploads/sponsors/'+response.banner_ads_2, function(err, response){});
                }
              }
            });
          }
          if(!_.isEmpty(_.get(resp, 'banner_ads_3'))){
            fs.rename('public/uploads/games/'+resp.banner_ads_3, 'public/uploads/sponsors/'+resp.banner_ads_3, function(err) {
              if (!err) {
                fs.unlink('public/uploads/games/'+resp.banner_ads_3, function(err, response){});
                if(response.banner_ads_3 && response.banner_ads_3 !=''){
                  fs.unlink('public/uploads/sponsors/'+response.banner_ads_3, function(err, response){});
                }
              }
            });
          }
          gameServices.getSponsorById(sponsorid).then(function(response){
            var data = {detail: response, status : true, };
            res.render('sponsors',{data: data, sponsorid:sponsorid, message : 'Record Updated successfully', error:'', page: 'Sponsor', title: 'Sponsor Management'});
          })
        })
        .catch(function(derr,err){
          gameServices.getSponsorById(sponsorid).then(function(response){
            data = {detail: response  ,  status : false};
            res.render('sponsors',{data: data, pages:'', page:'Sponsor', title: 'Sponsor Management', message : '', error:'Oops!! An error occured while processing request.Please try again'});
          })
        })  
      })
      .catch(function(derr,err){
        let tmpdata ={};
        if(derr == 'name')
        {
          tmpdata = {status : false,error: 'Sorry! name already exists.', message : ''};
        }
        else{
          tmpdata = {status : false, error: 'Oops!! An error occured while processing request.Please try again' , message : ''};
        }
        gameServices.getSponsorById(sponsorid).then(function(response){
          var data = {detail: response, status : true, };
          res.render('sponsors',{...tmpdata , data: data, sponsorid:sponsorid, page: 'Sponsor', title: 'Sponsor Management'});
        })
      })
    }
  })

  app.post('/admin/sponsors/:page?', isAuthenticated, upload.fields(sp_fields), function(req, res, next){
    var data = {};
    var isError = false;
    var page = req.params.page || 1;
    var condData = {};
    if(validator.isEmpty(_.get(req, 'body.name')))
    {
      data = {detail: []};
      res.render('sponsors',{data: data, status : false, message:'', pages:'', error: 'Name is required.', page:'Sponsor', title: 'Sponsor Management'});
      isError = true;
    }
    function checkSponsor(){
      return new Promise(function(resolve, reject){
        sponsor.getSponsorByName(_.get(req, 'body.name'), function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(!_.isEmpty(response))
            {
              reject('name', null)
            }
            else{
              resolve(response)
            }
          }
        })
      })
    }
    function createSponsor(){
      return new Promise(function(resolve, reject){
        var sponsorData = {
          name: req.body.name,
          description: req.body.description,
          status: req.body.status || 'active',
        };
        const filedata = req.files;
        if(filedata.logo && filedata.logo[0].filename)
        {
          sponsorData.logo = filedata.logo[0].filename;
        }
        if(filedata.banner_ads_1 && filedata.banner_ads_1[0].filename)
        {
          sponsorData.banner_ads_1 = filedata.banner_ads_1[0].filename;
        }
        if(filedata.banner_ads_2 && filedata.banner_ads_2[0].filename)
        {
          sponsorData.banner_ads_2 = filedata.banner_ads_2[0].filename;
        }
        if(filedata.banner_ads_3 && filedata.banner_ads_3[0].filename)
        {
          sponsorData.banner_ads_3 = filedata.banner_ads_3[0].filename;
        }
        sponsor.createData(sponsorData, function(err, response){
          if(err)
          {
              reject('noRecord', new Error(err));
          }
          else
          {
            resolve(response)
          }
        })
      })
    }
    if(!isError){
      checkSponsor()
      .then(function(response){
        createSponsor()
        .then(function(resp){
          if(!_.isEmpty(_.get(resp, 'logo'))){
            fs.rename('public/uploads/games/'+resp.logo, 'public/uploads/sponsors/'+resp.logo, function(err) {
              if (!err) {
                fs.unlink('public/uploads/games/'+resp.logo, function(err, response){});
              }
            });
          }
          if(!_.isEmpty(_.get(resp, 'banner_ads_1'))){
            fs.rename('public/uploads/games/'+resp.banner_ads_1, 'public/uploads/sponsors/'+resp.banner_ads_1, function(err) {
              if (!err) {
                fs.unlink('public/uploads/games/'+resp.banner_ads_1, function(err, response){});
              }
            });
          }
          if(!_.isEmpty(_.get(resp, 'banner_ads_2'))){
            fs.rename('public/uploads/games/'+resp.banner_ads_2, 'public/uploads/sponsors/'+resp.banner_ads_2, function(err) {
              if (!err) {
                fs.unlink('public/uploads/games/'+resp.banner_ads_2, function(err, response){});
              }
            });
          }
          if(!_.isEmpty(_.get(resp, 'banner_ads_3'))){
            fs.rename('public/uploads/games/'+resp.banner_ads_3, 'public/uploads/sponsors/'+resp.banner_ads_3, function(err) {
              if (!err) {
                fs.unlink('public/uploads/games/'+resp.banner_ads_3, function(err, response){});
              }
            });
          }
          gameServices.getSponsors(perPage, page, condData).then(function(response){
            data = {detail: response.sponsors};
            res.render('sponsors',{data: data, pages:'', status : true, message : 'sponsors fetched successfully', error:'', pages: response.pages, current: response.current, page:'Game', title: 'Game Management'});
          })
        })
        .catch(function(derr,err){
          gameServices.getSponsors(perPage, page, condData).then(function(response){
            data = {detail: response.sponsors  ,  status : false};
            res.render('sponsors',{data: data, pages:'', page:'Sponsor', title: 'Sponsor Management', message : '', error:'Oops!! An error occured while processing request.Please try again'});
          })
        })  
      })
      .catch(function(derr,err){
        if(derr == 'name')
        {
          data = {status : false,error: 'Sorry! name already exists.', message : ''};
        }
        else{
          data = {status : false, error: 'Oops!! An error occured while processing request.Please try again' , message : ''};
        }
        gameServices.getSponsors(perPage, page, condData).then(function(response){
          let mdata = {detail: response.sponsors};
          res.render('sponsors',{...data, data: mdata, pages: response.pages, current: response.current, page:'Sponsor', title: 'Sponsor Management', message : 'sponsors fetched successfully', error:''});
        })
      })
    }
  })

  app.get('/admin/sponsor/delete/:sponsorid', isAuthenticated, function(req, res, next){
    var sponsorid = req.params.sponsorid || null;
    function getSponsorById(){
      return new Promise(function(resolve, reject){
        sponsor.getSponsorById(sponsorid, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(_.isEmpty(response))
            {
              reject('sponsorNotFound', null)
            }
            else{
              resolve(response);
            }
          }
        })
      })
    }
    function removeSponsorById(){
      return new Promise(function(resolve, reject){
        sponsor.removeSponsorById(sponsorid, function(err, response){
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            resolve(response);
          }
        })
      })
    }
    if(sponsorid){
      getSponsorById()
      .then(function(response){
        
        removeSponsorById()
        .then(function(resp){  
          if(!_.isEmpty(_.get(response, 'logo'))){
            fs.unlink('public/uploads/sponsors/'+response.logo, function(err, response){
            });
          }
          res.redirect('/admin/sponsors');   
        })
        .catch(function(derr, err){
          res.redirect('/admin/sponsors');
        })
      })
      .catch(function(derr, err){
        res.redirect('/admin/sponsors');
      }) 
    }
    else
    {
      res.redirect('/admin/sponsors');
    }
  })

  // route for user logout
  app.get('/admin/logout', (req, res, next) => {
    if (req.session) {
      // delete session object
      req.session.destroy(function(err) {
        if(err) {
          return next(err);
        } else {
          return res.redirect('/admin');
        }
      });
    }
  });


/* Get Bet List*/
  app.get('/admin/bet/:page?', isAuthenticated, function(req, res, next){
    var perPage = 10;
    var page = req.params.page || 1
    let condData = {};


    function getBetData(){

      return new Promise(function(resolve, reject){
        // console.log('=======>', bet.getData(page, perPage, condData, function(err, response){err, response}));
        bet.getBetData(page, perPage, condData, function(err, response){

          console.log('err', err);
          console.log('response', response);
          if(err)
          {
            reject(null, new Error(err));
          }
          else
          {
            if(_.isEmpty(response))
            {
              reject('betList', null)
            }
            else{
              resolve(response);
            }
          }
        })
      })
    }
    getBetData()
    .then(function(response){
      var data = {detail: response.bets, token:'100101', status : 1, message : 'bets fetched successfully'};
      res.render('betlist',{data: data, pages: response.pages, current: response.current,  page: 'betList', title: 'Bet Listing'});
    
    })
    .catch(function(derr, err){
      if(derr == 'betList')
      {
        var data = {status : 0, error: err , pages:'', message : 'Sorry! This bet is not found'};
        var json_arr  = {data: data, success: false};
      }
      else{
        var data = {status : 0, error: err , pages:'', message : 'Oops!! An error occured while updating details.Please try again'};
        var json_arr  = {data: data, success: false};
      }
      res.render('betlist',{data: data, pages:'', page:'betList', title: 'Bet Listing'});
    }) 
  })
}