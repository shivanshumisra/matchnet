exports = module.exports = function(app, revealCommit, keccak256, numberToBN, abi){
	app.post('/getCommit', function(req, res){

		/** 
		 * this function creates the commit reveal pair to be stored in database at croupier bot. This should be integrated with
		 * an api that exposes only the commit number to the user just before placeBet
		 * we have to save it in DB with key -value as commit and reveal so whenever we asked for the "reveal" 
		 * the front end share the "commit" accordingly we will search for "reveal" and send.
		 * put in seperate api
		 */
		function createCommitReveal() {
			return new Promise((resolve, reject)=>{
				let revealEntropy = Math.random() * (new Date()).getTime() + keccak256(Math.random().toString());
			    let revealHash ='0x' + keccak256('0x'+abi.rawEncode(['string'], [revealEntropy]).toString('hex')).toString('hex');
			    let reveal = numberToBN(revealHash).toString(10);
			    let commitHash = '0x' + keccak256('0x' + abi.rawEncode(['uint256'], [reveal]).toString('hex')).toString('hex');
			    let commit = numberToBN(commitHash).toString(10);
			    // let blockNumber = await tronWeb.trx.getCurrentBlock();
			    var revealCommitData = new revealCommit({
			    	reveal : reveal,
			    	commit : commit,
			    	revealHash : revealHash,
			    	commitHash : commitHash
			    });
			    console.log(commit, reveal, revealCommitData);
			    revealCommit.createData(revealCommitData, (err, response) =>{
			    	if(err){
			    		reject(err)
			    	}
			    	else{
			    		var json = {"commit" : commit};
			    		resolve(json);
			    	}
			    })
			})
		    
		}
		createCommitReveal()
		.then((response)=>{
			var json_arr  = {data: response, success: true, message : 'Commit done successfully'};
		    res.contentType('application/json');
		    res.end(JSON.stringify(json_arr));
		})
		.catch((err)=>{
			var data = {status : 0, error: err , message : 'Oops!! An error occured while processing request.Please try again'};
			var json_arr  = {data: data, success: false};
			res.contentType('application/json');
			res.end(JSON.stringify(json_arr));
		})
	})
}