module.exports = function (app, game, sponsor, tournament) {
    var module = {};

    module.getGames = function (perPage, page, condData) {
    	condData.status = {'$ne' : 'Deleted'};
        return new Promise(function(resolve, reject){
	    game.getGameList(page, perPage, condData, function(err, response){
	      if(err)
	      {
	        reject(null, new Error(err));
	      }
	      else
	      {
	        resolve(response);
	      }
	    })
	  })
    };

    module.getGameById = function (gameid) {
        return new Promise(function(resolve, reject){
	    game.getGameById(gameid, function(err, response){
	      if(err)
	      {
	        reject(null, new Error(err));
	      }
	      else
	      {
	        resolve(response);
	      }
	    })
	  })
    };

    module.getSponsors = function (perPage, page, condData) {
    	condData.status = {'$ne' : 'Deleted'};
        return new Promise(function(resolve, reject){
	    sponsor.getSponsorList(page, perPage, condData, function(err, response){
	      if(err)
	      {
	        reject(null, new Error(err));
	      }
	      else
	      {
	        resolve(response);
	      }
	    })
	  })
    };

    module.getSponsorById = function (sponsorid) {
        return new Promise(function(resolve, reject){
	    sponsor.getSponsorById(sponsorid, function(err, response){
	      if(err)
	      {
	        reject(null, new Error(err));
	      }
	      else
	      {
	        resolve(response);
	      }
	    })
	  })
    };

    module.getTournamentById = function (tournamentid) {
        return new Promise(function(resolve, reject){
	    tournament.getTournamentById(tournamentid, function(err, response){
	      if(err)
	      {
	        reject(null, new Error(err));
	      }
	      else
	      {
	        resolve(response);
	      }
	    })
	  })
    };

    module.getTournaments = function (perPage, page, condData) {
    	condData.status = {'$ne' : 'Deleted'};
        return new Promise(function(resolve, reject){
	    tournament.getTournamentList(page, perPage, condData, function(err, response){
	      if(err)
	      {
	        reject(null, new Error(err));
	      }
	      else
	      {
	        resolve(response);
	      }
	    })
	  })
    };

    return module;
};